<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Page
 * @copyright  Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Top menu block
 *
 * @category    Mage
 * @package     Mage_Page
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Cmsmart_Megamenu_Block_Html_Topmenu extends Mage_Page_Block_Html_Topmenu
{

    /**
     * Recursively generates top menu html from data that is specified in $menuTree
     *
     * @param Varien_Data_Tree_Node $menuTree
     * @param string $childrenWrapClass
     * @return string
     * @deprecated since 1.8.2.0 use child block catalog.topnav.renderer instead
     */
    protected function _getHtml(Varien_Data_Tree_Node $menuTree, $childrenWrapClass)
    {
        $html = '';

        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = is_null($parentLevel) ? 0 : $parentLevel + 1;

        $counter = 1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';

        foreach ($children as $child) {

            $child->setLevel($childLevel);
            $child->setIsFirst($counter == 1);
            $child->setIsLast($counter == $childrenCount);
            $child->setPositionClass($itemPositionClassPrefix . $counter);

            $outermostClassCode = '';
            $outermostClass = $menuTree->getOutermostClass();

            if ($childLevel == 0 && $outermostClass) {
                $outermostClassCode = ' class="' . $outermostClass . '" ';
                $child->setClass($outermostClass);
            }
            $parentClass = '';
            $catId = str_replace('category-node-', '', $child->getId());
            $megamenu = Mage::helper('megamenu')->Megamenu($catId);
            if($this->getBlockLeft($megamenu)){
                $parentClass = 'parent';
            }
            $html .= '<li ' . $this->_getRenderedMenuItemAttributes($child) .'>';
            $html .= '<a href="' . $child->getUrl() . '" ' . $outermostClassCode . '><span>'
                . $this->escapeHtml($child->getName()) . '</span></a>';


            if($this->getBlockLeft($megamenu)) {
                $classblock =' menu-content';
            }
            //var_dump($this->getBlockLeft($megamenu));
            //echo $this->getBlockLeft($megamenu);
            if($this->getBlockLeft($megamenu)) {

                if (!empty($childrenWrapClass)) {
                    $html .= '<div class="' . $childrenWrapClass . '">';
                }
                $html .= '<ul class="level' . $childLevel . '">';
                $html .= $this->getBlockLeft($megamenu);
                $html .= '</ul>';
                if (!empty($childrenWrapClass)) {
                    $html .= '</div>';
                }
            }
            if ($child->hasChildren()) {
                if (!empty($childrenWrapClass)) {
                    $html .= '<div class="' . $childrenWrapClass . '">';
                }
                $html .= '<ul class="level' . $childLevel . '">';
                $html .= $this->_getHtml($child, $childrenWrapClass);
                $html .= '</ul>';

                if (!empty($childrenWrapClass)) {
                    $html .= '</div>';
                }
            }
            $html .= '</li>';

            $counter++;
        }

        return $html;
    }

    public function getBlockLeft($block){
        $active_static_block = $block[0]['active_static_block'];
        $active_static_block_left = $block[0]['active_static_block_left'];
        $id =  $block[0]['category_id'];
        $idblock = $block[0]['static_block_left'];
        $showblock = $this->getShowblockleft($active_static_block_left,$idblock,$id);
        if($active_static_block == 1){
            return $showblock;
        }else {
            return ;
        }
    }

    public function getShowblockleft($active,$id,$cid) {
        if($active == 1){
            $width =  Mage::helper('megamenu')->Megamenu($cid);
            $auto = $width[0]['auto_width'];
            $blockIdentifier = Mage::getModel('cms/block')->load($id)->getIdentifier();
            if($auto == 0) {
                $widthleft=  $width[0]['width_block_left'];
                return '<div class="static-block-left" style="width:'.$widthleft.'px; float: left; margin-top: 5px; margin-right: 10px;">'.$this->getLayout()->createBlock('cms/block')->setBlockId($blockIdentifier)->toHtml().'</div>';
            } else {
                return '<div class="static-block-left">'.$this->getLayout()->createBlock('cms/block')->setBlockId($blockIdentifier)->toHtml().'</div>';
            }
        }   else {
            return ;
        }
    }

    /**
     * Returns array of menu item's classes
     *
     * @param Varien_Data_Tree_Node $item
     * @return array
     */
    protected function _getMenuItemClasses(Varien_Data_Tree_Node $item)
    {
        $classes = array();

        $classes[] = 'level' . $item->getLevel();
        $classes[] = $item->getPositionClass();

        $catId = str_replace('category-node-', '', $item->getId());
        $megamenu = Mage::helper('megamenu')->Megamenu($catId);

        if ($item->getIsFirst()) {
            $classes[] = 'first';
        }

        if ($item->getIsActive()) {
            $classes[] = 'active';
        }

        if ($item->getIsLast()) {
            $classes[] = 'last';
        }

        if ($item->getClass()) {
            $classes[] = $item->getClass();
        }

        if ($item->hasChildren() OR $this->getBlockLeft($megamenu)) {
            $classes[] = 'parent';
        }

        return $classes;
    }

    /**
     * Retrieve cache key data
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        $shortCacheId = array(
            'TOPMENU',
            Mage::app()->getStore()->getId(),
            Mage::getDesign()->getPackageName(),
            Mage::getDesign()->getTheme('template'),
            Mage::getSingleton('customer/session')->getCustomerGroupId(),
            'template' => $this->getTemplate(),
            'name' => $this->getNameInLayout(),
            $this->getCurrentEntityKey()
        );
        $cacheId = $shortCacheId;

        $shortCacheId = array_values($shortCacheId);
        $shortCacheId = implode('|', $shortCacheId);
        $shortCacheId = md5($shortCacheId);

        $cacheId['entity_key'] = $this->getCurrentEntityKey();
        $cacheId['short_cache_id'] = $shortCacheId;

        return $cacheId;
    }

    /**
     * Retrieve current entity key
     *
     * @return int|string
     */

}
