<?php

class Consol_Categoryblocks_Block_Adminhtml_Categoryblocks extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    protected function _construct()
    {
        parent::_construct();
        $helper = Mage::helper('consol_categoryblocks');
        $this->_blockGroup = 'consol_categoryblocks';
        $this->_controller = 'adminhtml_categoryblocks';

        $this->_headerText = $helper->__('Categoryblocks Management');

        $this->_addButtonLabel = $helper->__('Add Slider');
    }
}

?>