<?php

class Consol_Categoryblocks_Block_Adminhtml_Categoryblocks_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        $helper = Mage::helper('consol_categoryblocks');

        parent::__construct();
        $this->setId('department_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($helper->__('Category Slider Information'));
    }

    protected function _prepareLayout()
    {
        $helper = Mage::helper('consol_categoryblocks');

        $this->addTab('general_section', array(
            'label' => $helper->__('General Information'),
            'title' => $helper->__('General Information'),
            'content' => $this->getLayout()->createBlock('consol_categoryblocks/adminhtml_categoryblocks_edit_tabs_general')->toHtml(),
        ));

        return parent::_prepareLayout();
    }

}