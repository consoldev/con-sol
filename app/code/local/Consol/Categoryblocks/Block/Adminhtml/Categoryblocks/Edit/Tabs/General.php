<?php

class Consol_Categoryblocks_Block_Adminhtml_Categoryblocks_Edit_Tabs_General extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {

        $helper = Mage::helper('consol_categoryblocks');
        $model = Mage::registry('current_categoryblocks_slider');

        $storeId = Mage::app()->getRequest()->getParam('store');
        if(!$storeId){
            $storeId = Mage::app()->getDefaultStoreView()->getId();
        }
        $model['store_id'] = $storeId;

        $form = new Varien_Data_Form();
        $fieldset = $form->addFieldset('custom_form', array(
            'legend' => $helper->__('General Information')
        ));

        $fieldset->addField('name', 'text', array(
            'label' => $helper->__('Name'),
            'required' => true,
            'name' => 'name',
        ));

        $fieldset->addField('category_id', 'select', array(
            'label' => $helper->__('Category'),
            'name' => 'category_id',
            'values' => Mage::helper('consol_categoryblocks')->getAllCategoriesArray(true)
        ));
        $fieldset->addField('block_filter_link', 'text', array(
            'label' => $helper->__('Link'),
            'name' => 'block_filter_link',
        ));

        $fieldset->addField('store_id', 'hidden', array(
            'label' => $helper->__('store_id'),
            'name' => 'store_id',
        ));

        $fieldset->addField('order', 'text', array(
            'label' => $helper->__('Partner order'),
            'required' => true,
            'name' => 'order',
        ));
        $fieldset->addField('status', 'select', array(
            'label' => $helper->__('Status'),
            'required' => false,
            'name' => 'status',
            'values' =>array(
                array(
                    'value'     => 1,
                    'label'     => $helper->__('Enable'),
                ),
                array(
                    'value'     => 0,
                    'label'     => $helper->__('Disable'),
                )),
            'default' => '0',
        ));

        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }



}

