<?php

class Consol_Categoryblocks_Block_Adminhtml_Categoryblocks_Grid extends Mage_Adminhtml_Block_Widget_Grid
{


    protected function _prepareCollection()
    {
        $storeId = Mage::app()->getRequest()->getParam('store');

        if(!$storeId){
            $storeId = Mage::app()->getDefaultStoreView()->getId();
        }
        if($storeId) {
            $collectionMain = Mage::getModel('consol_categoryblocks/categoryblocks')->getCollection()
                                                                              ->joinAdditional()
                                                                              ->addFieldToFilter('store_id', $storeId);
            $this->setCollection($collectionMain);
            return parent::_prepareCollection();
        }
    }

    protected function _prepareColumns()
    {

        $helper = Mage::helper('consol_categoryblocks');

        $this->addColumn('entity_id', array(
            'header' => $helper->__('Id'),
            'index' => 'entity_id'
        ));

        $this->addColumn('name', array(
            'header' => $helper->__('Name'),
            'index' => 'name'
        ));

        $this->addColumn('category_id', array(
            'header' => $helper->__('Category'),
            'index' => 'category_id',
            'frame_callback' => array($this, 'getCatName'),
        ));
        $this->addColumn('block_filter_link', array(
            'header' => $helper->__('Slider link'),
            'index' => 'block_filter_link',
        ));

        $this->addColumn('order', array(
            'header' => $helper->__('Partner order'),
            'index' => 'order',
        ));

        $this->addColumn('status', array(
            'header' => $helper->__('Status'),
            'index' => 'status',
            'frame_callback' => array($this, 'showStatus'),
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
        ));

        return $this;
    }

    public function getRowUrl($model)
    {
        return $this->getUrl('*/*/edit', array(
            'id' => $model->getEntityId(),
        ));
    }

    public function showStatus($status){
        if($status){
            switch($status) {
                case 0:
                    return 'Disable';
                    break;
                case 1:
                    return 'Enable';
                    break;

                default:
                    return 'Disable';
            }
        }else{
            return 'Disable';
        }

    }

    public function getCatName($catId)
    {
        $category = Mage::getModel('catalog/category')->load($catId);

        return $category->getName();
    }

}