<?php

class Consol_Categoryblocks_Block_Adminhtml_Slideritems_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    protected function _construct()
    {
        $this->_blockGroup = 'consol_categoryblocks';
        $this->_controller = 'adminhtml_slideritems';
    }

    public function getHeaderText()
    {
        $helper = Mage::helper('consol_categoryblocks');
        $model = Mage::registry('current_categoryblocks_slider_item');

        if ($model->getId()) {
            return $helper->__("Edit slider item info", $this->escapeHtml($model->getName()));
        } else {
            return $helper->__("Add item");
        }
    }

}