<?php

class Consol_Categoryblocks_Block_Adminhtml_Slideritems_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        $helper = Mage::helper('consol_categoryblocks');

        parent::__construct();
        $this->setId('slideritems_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($helper->__('Category Slider`s Item Information'));
    }

    protected function _prepareLayout()
    {
        $helper = Mage::helper('consol_categoryblocks');

        $this->addTab('general_section', array(
            'label' => $helper->__('General Information'),
            'title' => $helper->__('General Information'),
            'content' => $this->getLayout()->createBlock('consol_categoryblocks/adminhtml_slideritems_edit_tabs_general')->toHtml(),
        ));

        return parent::_prepareLayout();
    }

}