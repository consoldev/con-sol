<?php

class Consol_Categoryblocks_Block_Adminhtml_Slideritems_Edit_Tabs_General extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {

        $helper = Mage::helper('consol_categoryblocks');
        $model = Mage::registry('current_categoryblocks_slider_item');

        $storeId = Mage::app()->getRequest()->getParam('store');
        if(!$storeId){
            $storeId = Mage::app()->getDefaultStoreView()->getId();
        }
        $model['store_id'] = $storeId;

        $form = new Varien_Data_Form();
        $fieldset = $form->addFieldset('custom_form', array(
            'legend' => $helper->__('General Information')
        ));

        $fieldset->addField('item_title', 'text', array(
            'label' => $helper->__('Item Label'),
            'required' => true,
            'name' => 'item_title',
        ));

        $fieldset->addField('name', 'text', array(
            'label' => $helper->__('Name'),
            'required' => true,
            'name' => 'name',
        ));

        $fieldset->addField('slider_id', 'select', array(
            'label' => $helper->__('Slider'),
            'name' => 'slider_id',
            'values' => Mage::helper('consol_categoryblocks')->getAllSlidersArray(true)
        ));

        $fieldset->addField('store_id', 'hidden', array(
            'label' => $helper->__('store_id'),
            'name' => 'store_id',
        ));

        $fieldset->addField('price', 'text', array(
            'label' => $helper->__('Price range'),
            'required' => true,
            'name' => 'price',
        ));

        $fieldset->addField('lable', 'select', array(
            'label' => $helper->__('Label'),
            'required' => true,
            'name' => 'lable',
            'values' => array(
                '1' => $this->__('Новые'),
                '2' => $this->__('Б/у')
            )
        ));

        $fieldset->addField('link', 'text', array(
            'label' => $helper->__('Link'),
            'required' => true,
            'name' => 'link',
        ));

        $fieldset->addField('logo', 'image', array(
            'label' => $helper->__('Logo'),
            'required' => false,
            'name' => 'logo',
            'after_element_html' => $this->getImageHtml('logo', $model->getLogo())
        ));

        $fieldset->addField('order', 'text', array(
            'label' => $helper->__('Partner order'),
            'required' => true,
            'name' => 'order',
        ));

        $fieldset->addField('status', 'select', array(
            'label' => $helper->__('Status'),
            'required' => false,
            'name' => 'status',
            'values' =>array(
                array(
                    'value'     => 1,
                    'label'     => $helper->__('Enable'),
                ),
                array(
                    'value'     => 0,
                    'label'     => $helper->__('Disable'),
                )),
            'default' => '0',
        ));

        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }

    protected function getImageHtml($field, $img)
    {
        $html = '';
        if ($img) {
            $html .= '<p style="margin-top: 5px">';
            $html .= '<img style="max-width:300px" src="' . Mage::getBaseUrl('media') . 'slideritems'. DS . $img . '">';
            $html .= '<input type="hidden" value="' . $img . '" name="old_' . $field . '"/>';
            $html .= '</p>';
        }

        return $html;
    }

}

