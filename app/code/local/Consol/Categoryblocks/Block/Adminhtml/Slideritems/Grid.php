<?php

class Consol_Categoryblocks_Block_Adminhtml_Slideritems_Grid extends Mage_Adminhtml_Block_Widget_Grid
{


    protected function _prepareCollection()
    {
        $storeId = Mage::app()->getRequest()->getParam('store');

        if(!$storeId){
            $storeId = Mage::app()->getDefaultStoreView()->getId();
        }
        if($storeId) {
            $collectionMain = Mage::getModel('consol_categoryblocks/item')->getCollection()
                                                                              ->joinAdditional()
                                                                              ->addFieldToFilter('store_id', $storeId);
            $this->setCollection($collectionMain);
            return parent::_prepareCollection();
        }
    }

    protected function _prepareColumns()
    {

        $helper = Mage::helper('consol_categoryblocks');

        $this->addColumn('entity_id', array(
            'header' => $helper->__('Id'),
            'index' => 'entity_id'
        ));

        $this->addColumn('item_title', array(
            'header' => $helper->__('Item Label'),
            'index' => 'item_title'
        ));

        $this->addColumn('name', array(
            'header' => $helper->__('Name'),
            'index' => 'name'
        ));

        $this->addColumn('slider_id', array(
            'header' => $helper->__('Slider'),
            'index' => 'slider_id',
            'frame_callback' => array($this, 'getSliderName'),
        ));

        $this->addColumn('price', array(
            'header' => $helper->__('Price range'),
            'index' => 'price'
        ));

        $this->addColumn('lable', array(
            'header' => $helper->__('Label'),
            'index' => 'lable',
            'frame_callback' => array($this, 'showLabel')
        ));

        $this->addColumn('link', array(
            'header' => $helper->__('Link'),
            'index' => 'link'
        ));

        $this->addColumn('logo', array(
            'header' => $helper->__('Logo'),
            'index' => 'logo',
            'frame_callback' => array($this, 'callback_image'),
        ));

        $this->addColumn('order', array(
            'header' => $helper->__('Partner order'),
            'index' => 'order',
        ));

        $this->addColumn('status', array(
            'header' => $helper->__('Status'),
            'index' => 'status',
            'frame_callback' => array($this, 'showStatus'),
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
        ));

        return $this;
    }

    public function getRowUrl($model)
    {
        return $this->getUrl('*/*/edit', array(
            'id' => $model->getEntityId(),
        ));
    }

    public function showStatus($status){
        if($status){
            switch($status) {
                case 0:
                    return 'Disable';
                    break;
                case 1:
                    return 'Enable';
                    break;

                default:
                    return 'Disable';
            }
        }else{
            return 'Disable';
        }

    }

    public function showLabel($label){
        if($label){
            switch($label) {
                case 1:
                    return $this->__('Новые');
                    break;
                case 2:
                    return $this->__('Б/у');
                    break;
            }
        }
    }

    public function getCatName($catId)
    {
        $category = Mage::getModel('catalog/category')->load($catId);

        return $category->getName();
    }

    public function getSliderName($sliderId)
    {
        $slider = Mage::getModel('consol_categoryblocks/info')->load($sliderId, 'entity_id');

        return $slider->getName();
    }

    public function callback_image($value)
    {
        $width = 70;
        $height = 70;
        if($value) {
            return "<img src='" . Mage::getBaseUrl('media') . 'slideritems' . DS . $value . "' width=" . $width . " height=" . $height . "/>";
        }
    }

}