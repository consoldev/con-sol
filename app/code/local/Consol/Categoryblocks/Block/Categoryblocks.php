<?php

class Consol_Categoryblocks_Block_Categoryblocks extends Mage_Core_Block_Template
{
    public function getCategoryblocksCollection(){

        $storeId = Mage::app()->getStore()->getStoreId();
        $collectionMain = Mage::getModel('consol_categoryblocks/categoryblocks')->getCollection()
            ->joinAdditional()
            ->setOrder('main_table.order', 'asc')
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('store_id', $storeId);

        if($collectionMain->getSize() == 0){
            $defaultStoreId = Mage::app()
                ->getWebsite()
                ->getDefaultGroup()
                ->getDefaultStoreId();
            $collectionMain = Mage::getModel('consol_categoryblocks/categoryblocks')->getCollection()
                ->joinAdditional()
                ->setOrder('main_table.order', 'asc')
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('store_id', $defaultStoreId);
        }

        return $collectionMain;
    }



}

?>