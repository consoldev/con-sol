<?php

class Consol_Categoryblocks_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getJsonCategoryblocks(){
        $storeId = Mage::app()->getStore()->getStoreId();
        $data =array();
        $collectionMain = Mage::getModel('consol_categoryblocks/categoryblocks')->getCollection()
            ->joinAdditional()
            ->setOrder('main_table.order', 'asc')
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('store_id', $storeId);
        if($collectionMain->getSize() == 0){
            $defaultStoreId = Mage::app()
                ->getWebsite()
                ->getDefaultGroup()
                ->getDefaultStoreId();
            $collectionMain = Mage::getModel('consol_categoryblocks/categoryblocks')->getCollection()
                ->joinAdditional()
                ->setOrder('main_table.order', 'asc')
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('store_id', $defaultStoreId);
        }
        foreach ($collectionMain as $item){
            $data[$item->getEntityId()]['id'] = $item->getEntityId();
            $data[$item->getEntityId()]['name'] = $item->getName();
            $data[$item->getEntityId()]['phone'] = $item->getPhone();
            $data[$item->getEntityId()]['alias'] = $item->getAlias();
            $data[$item->getEntityId()]['email'] = $item->getEmail();
        }

        return json_encode($data);
    }


    public function getEmail($alias)
    {
        $collection = Mage::getModel('consol_categoryblocks/categoryblocks')->load($alias, 'alias');
        return $collection;
    }

    public function getContact($id)
    {
        $collection = Mage::getModel('consol_categoryblocks/categoryblocks')->load($id, 'entity_id');
        return $collection;
    }

    public function deleteSliderItemImage($model)
    {
        $path = Mage::getBaseDir('media') . DS . 'slideritems' . DS;
        $name = $model->getLogo();
        if(file_exists($path.$name)){
            unlink($model->getEntityId . $path . $name);
        }

    }

    public function getSliderItemImage($model)
    {
        $name = $model->getLogo();

        $url = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA).DS.'slideritems'.DS;
        if(file_exists($url.$name)){
            return $url.$name;
        }
    }

    public function getAllSlidersArray($optionList = false){
        $storeId = Mage::app()->getRequest()->getParam('store');
        if(!$storeId){
            $storeId = Mage::app()->getDefaultStoreView()->getId();
        }
        $collectionMain = Mage::getModel('consol_categoryblocks/categoryblocks')->getCollection()
            ->joinAdditional()
            ->setOrder('main_table.order', 'asc')
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('store_id', 1);

        if (!$optionList) {
            return $collectionMain;
        }
        foreach ($collectionMain as $sliderId => $slider) {
            $sliderName = $slider->getName();
            if (isset($sliderName)) {
                $sliders[] = array(
                    'value' => $sliderId,
                    'label' => Mage::helper('consol_categoryblocks')->__($sliderName)
                );
            }
        }
        return $sliders;
    }

    public function getSliderByCategoryId($catId, $curStore=false){
        if($curStore == false) {
            $storeId = Mage::app()->getRequest()->getParam('store');
            if (!$storeId) {
                $storeId = Mage::app()->getDefaultStoreView()->getId();
            }
        }else{
            $storeId = $curStore;
        }

        $slider = Mage::getModel('consol_categoryblocks/categoryblocks')->getCollection()
            ->joinAdditional()
            ->setOrder('main_table.order', 'asc')
            ->addFieldToFilter('category_id', $catId)
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('store_id', $storeId);

        if($slider->getSize() == 0){
            $defaultStoreId = Mage::app()
                ->getWebsite()
                ->getDefaultGroup()
                ->getDefaultStoreId();
            $slider = Mage::getModel('consol_categoryblocks/categoryblocks')->getCollection()
                ->joinAdditional()
                ->setOrder('main_table.order', 'asc')
                ->addFieldToFilter('category_id', $catId)
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('store_id', $defaultStoreId);
        }
        return $slider;
    }

    public function getItemsBySliderId($sliderId, $curStore=false){
        if($curStore == false) {
            $storeId = Mage::app()->getRequest()->getParam('store');
            if (!$storeId) {
                $storeId = Mage::app()->getDefaultStoreView()->getId();
            }
        }else{
            $storeId = $curStore;
        }
        $collectionMain = Mage::getModel('consol_categoryblocks/item')->getCollection()
            ->joinAdditional()
            ->setOrder('main_table.order', 'asc')
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('slider_id', $sliderId)
            ->addFieldToFilter('store_id', $storeId);

        if($collectionMain->getSize() == 0){
            $defaultStoreId = Mage::app()
                ->getWebsite()
                ->getDefaultGroup()
                ->getDefaultStoreId();
            $collectionMain = Mage::getModel('consol_categoryblocks/item')->getCollection()
                ->joinAdditional()
                ->setOrder('main_table.order', 'asc')
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('slider_id', $sliderId)
                ->addFieldToFilter('store_id', $defaultStoreId);
            }
        return $collectionMain;
    }

    public function getAllSliderItemsArray($optionList = false){
        $storeId = Mage::app()->getRequest()->getParam('store');
        if(!$storeId){
            $storeId = Mage::app()->getDefaultStoreView()->getId();
        }
        $collectionMain = Mage::getModel('consol_categoryblocks/item')->getCollection()
            ->joinAdditional()
            ->setOrder('main_table.order', 'asc')
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('store_id', $storeId);

        if($collectionMain->getSize() == 0){
            $defaultStoreId = Mage::app()
                ->getWebsite()
                ->getDefaultGroup()
                ->getDefaultStoreId();
            $collectionMain = Mage::getModel('consol_categoryblocks/item')->getCollection()
                ->joinAdditional()
                ->setOrder('main_table.order', 'asc')
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('store_id', $defaultStoreId);
        }
        if (!$optionList) {
            return $collectionMain;
        }
        foreach ($collectionMain as $sliderId => $slider) {
            $sliderName = $slider->getName();
            if (isset($sliderName)) {
                $sliders[] = array(
                    'value' => $sliderId,
                    'label' => Mage::helper('consol_categoryblocks')->__($sliderName)
                );
            }
        }
        return $sliders;
    }

    public function getAllCategoriesArray($optionList = false)
    {
        $categoriesArray = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('id')
            ->addAttributeToSort('path', 'asc')
            ->addFieldToFilter('is_active', array('eq'=>'1'))
            ->addFieldToFilter('parent_id', array('eq'=>'2'))
            ->load()
            ->toArray();

        if (!$optionList) {
            return $categoriesArray;
        }

        foreach ($categoriesArray as $categoryId => $category) {
            if (isset($category['name'])) {
                $categories[] = array(
                    'value' => $category['entity_id'],
                    'label' => Mage::helper('consol_categoryblocks')->__($category['name'])
                );
            }
        }

        return $categories;
    }


}

?>