<?php

class Consol_Categoryblocks_Model_Resource_Categoryblocks_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('consol_categoryblocks/categoryblocks');
    }


    public function joinAdditional()
    {
        $this->join(
            array('info' => 'consol_categoryblocks/info'),
            'main_table.entity_id = info.entity_id',
            array('name', 'store_id')
        );
        return $this;
    }
}

