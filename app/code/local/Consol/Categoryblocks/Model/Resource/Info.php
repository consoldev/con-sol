<?php

class Consol_Categoryblocks_Model_Resource_Info extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('consol_categoryblocks/info', 'id');
    }
}