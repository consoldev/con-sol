<?php

class Consol_Categoryblocks_Model_Resource_Infoitem extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('consol_categoryblocks/infoitem', 'id');
    }
}