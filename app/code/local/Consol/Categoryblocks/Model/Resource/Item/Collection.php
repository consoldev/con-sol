<?php

class Consol_Categoryblocks_Model_Resource_Item_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('consol_categoryblocks/item');
    }


    public function joinAdditional()
    {
        $this->join(
            array('infoitem' => 'consol_categoryblocks/infoitem'),
            'main_table.entity_id = infoitem.entity_id',
            array('item_title', 'name', 'price', 'lable', 'store_id')
        );
        return $this;
    }
}

