<?php

class Consol_Categoryblocks_Adminhtml_SlideritemsController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        $this->loadLayout()->_setActiveMenu('categoryitem');
        $this->_addContent($this->getLayout()->createBlock('consol_categoryblocks/adminhtml_slideritems'));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {

        $storeId = Mage::app()->getRequest()->getParam('store');
        if(!$storeId){
            $storeId = Mage::app()->getDefaultStoreView()->getId();
        }
        $id = (int) $this->getRequest()->getParam('id');

        $modelMain = Mage::getModel('consol_categoryblocks/item')
            ->getCollection()
            ->addFieldToFilter('entity_id', $id)
            ->getFirstItem();

        $modelInfo = Mage::getModel('consol_categoryblocks/infoitem')
            ->getCollection()
            ->addFieldToFilter('entity_id', $id)
            ->addFieldToFilter('store_id', $storeId)
            ->getFirstItem();
        $modelMain->setName($modelInfo->getName())->setPrice($modelInfo->getPrice())->setLable($modelInfo->getLable())->setItemTitle($modelInfo->getItemTitle());
        Mage::register('current_categoryblocks_slider_item', $modelMain);
        $this->loadLayout()->_setActiveMenu('categoryblocks');
        $this->_addLeft($this->getLayout()->createBlock('consol_categoryblocks/adminhtml_slideritems_edit_tabs'));
        $this->_addContent($this->getLayout()->createBlock('consol_categoryblocks/adminhtml_slideritems_edit'));

        $this->renderLayout();
    }

    public function saveAction()
    {

        if ($data = $this->getRequest()->getPost()) {
            try {
                $helper = Mage::helper('consol_categoryblocks');
                $modelMain = Mage::getModel('consol_categoryblocks/item');
                $modelInfo = Mage::getModel('consol_categoryblocks/infoitem');
                $id = $this->getRequest()->getParam('id');
                $main = $modelMain->load($id);
                $mainId = $main->getEntityId();

                if (isset($mainId)) {
                    $id = $main->getEntityId();
                    if (isset($data['logo'])) {
                        if(isset($data['logo']['delete'])){
                            if ($helper->getSliderItemImage($modelMain)) {
                                $helper->deleteSliderItemImage($modelMain);
                                $data['logo']['value'] = '';
                            }
                        }
                        $image = $data['logo']['value'];
                        $data['logo'] = $image;
                    }

                    $main->setData($data)->setEntityId($id);
                    $main->save();

                    $data['entity_id'] = $main->getEntityId();
                    $info = $modelInfo
                        ->getCollection()
                        ->addFieldToFilter('entity_id', $id)
                        ->addFieldToFilter('store_id', $data['store_id'])
                        ->getFirstItem();

                    if($info->getId()){
                        $store = $info->getStoreId();
                        $infoId = $info->getId();
                        if($infoId){
                            if(!($data['store_id'])) {
                                $data['store_id'] = Mage::app()->getDefaultStoreView()->getId();
                            }
                            if ($store == $data['store_id']) {
                                $info->setData($data)->setId($infoId);

                                $info->save();
                            }else{
                                $info->setData($data);
                                $info->save();
                            }
                        }
                    } else {
                        $info->setData($data);
                        $info->save();
                    }

                } else {
                    $modelMain->setData($data);
                    $modelMain->save();
                    $data['store_id'] = Mage::app()->getDefaultStoreView()->getId();
                    $data['entity_id'] = $modelMain->getEntityId();
                    $modelInfo->setData($data);
                    $modelInfo->save();
                }

                if (isset($_FILES['logo']['name']) && $_FILES['logo']['name'] != '') {
                    try {
                        $helper = Mage::helper('consol_categoryblocks');
                        $uploader = new Varien_File_Uploader('logo');
                        $name = explode('.', $_FILES['logo']['name']);
                        $allowedExtentions = array('jpg', 'jpeg', 'gif', 'png', 'JPG', 'JPEG', 'GIF', 'PNG');

                        if(!in_array($name[1], $allowedExtentions)){
                            Mage::getSingleton('adminhtml/session')->addError($this->__('You choose image with wrong extension. Allowed extensions: jpg, jpeg, gif, png'));

                            $this->_redirect('*/*/edit', array(
                                'id' => $this->getRequest()->getParam('id')));
                            return;
                        }

                        $uploader->setAllowedExtensions($allowedExtentions);
                        $uploader->setAllowRenameFiles(false);

                        $uploader->setFilesDispersion(false);

                        $path = Mage::getBaseDir('media') . DS . 'slideritems' . DS;
                        preg_match('/\.([^\.]+)$/', $_FILES['logo']['name'], $extension);

                        if ($modelMain->getLogo()) {
                            if ($helper->getSliderItemImage($modelMain)) {

                                $helper->deleteSliderItemImage($modelMain);
                            }
                        }
                        $uploader->save($path, $modelMain->getEntityId() . $name[0] . strtolower($extension[0]));
                        $modelMain->setLogo($uploader->getUploadedFileName());

                        $modelMain->save();

                    } catch (Exception $e) {
                    }

                }
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Information was saved successfully'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array(
                    'id' => $this->getRequest()->getParam('id')
                ));
            }
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $helper = Mage::helper('consol_categoryblocks');
                $modelInfo = Mage::getModel('consol_categoryblocks/infoitem')->getCollection()
                    ->addFieldToFilter('entity_id', $id);
                foreach($modelInfo as $model){
                    $model->delete();
                }

                $checkMore = Mage::getModel('consol_categoryblocks/infoitem')->load($id, 'entity_id');
                if(!$checkMore->getId()) {
                    $mainModel = Mage::getModel('consol_categoryblocks/item')->load($id);
                    if ($mainModel->getLogo() && $helper->getSliderItemImage($mainModel)) {
                        $helper->deleteSliderItemImage($mainModel);
                    }
                    $mainModel->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Record was deleted successfully'));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $ids = $this->getRequest()->getParam('id', null);

        if (is_array($ids) && sizeof($ids) > 0) {

            try {
                $helper = Mage::helper('consol_categoryblocks');

                foreach ($ids as $id) {
                    $modelInfo = Mage::getModel('consol_categoryblocks/infoitem')->getCollection()
                        ->addFieldToFilter('entity_id', $id);
                    foreach($modelInfo as $model){

                        $model->delete();
                    }
                    $mainModel = Mage::getModel('consol_categoryblocks/item')->load($id);
                    if ($mainModel->getLogo()) {
                        if ($helper->getSliderItemImage($mainModel)) {

                            $helper->deleteSliderItemImage($mainModel);
                        }
                    }
                    $mainModel->delete();
                    $mainModel->delete();
                }

                $this->_getSession()->addSuccess($this->__('Total of %d records have been deleted', sizeof($ids)));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        } else {
            $this->_getSession()->addError($this->__('Please select records'));
        }
        $this->_redirect('*/*');
    }

}

?>