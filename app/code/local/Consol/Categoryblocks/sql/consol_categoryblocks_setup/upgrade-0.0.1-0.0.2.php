<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('consol_categoryblocks_iteminfo'),'item_title', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => false,
        'length'    => 255,
        'after'     => 'store_id', // column name to insert new column after
        'comment'   => 'Item Label'
    ));
$installer->endSetup();
?>