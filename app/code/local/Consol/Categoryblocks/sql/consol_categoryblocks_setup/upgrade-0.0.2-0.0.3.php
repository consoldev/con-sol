<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('consol_categoryblocks'),'block_filter_link', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => false,
        'length'    => 255,
        'after'     => 'category_id', // column name to insert new column after
        'comment'   => 'Slider filter link'
    ));
$installer->endSetup();
?>