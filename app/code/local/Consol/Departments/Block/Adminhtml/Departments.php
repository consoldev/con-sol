<?php

class Consol_Departments_Block_Adminhtml_Departments extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    protected function _construct()
    {
        parent::_construct();
        $helper = Mage::helper('consol_departments');
        $this->_blockGroup = 'consol_departments';
        $this->_controller = 'adminhtml_departments';

        $this->_headerText = $helper->__('Departments Management');

        $this->_addButtonLabel = $helper->__('Add Department');
    }
}

?>