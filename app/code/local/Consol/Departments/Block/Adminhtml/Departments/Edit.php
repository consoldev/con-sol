<?php

class Consol_Departments_Block_Adminhtml_Departments_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    protected function _construct()
    {
        $this->_blockGroup = 'consol_departments';
        $this->_controller = 'adminhtml_departments';
    }

    public function getHeaderText()
    {
        $helper = Mage::helper('consol_departments');
        $model = Mage::registry('current_department');

        if ($model->getId()) {
            return $helper->__("Edit department info", $this->escapeHtml($model->getName()));
        } else {
            return $helper->__("Add Department item");
        }
    }

}