<?php

class Consol_Departments_Block_Adminhtml_Departments_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        $helper = Mage::helper('consol_departments');

        parent::__construct();
        $this->setId('department_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($helper->__('Department Information'));
    }

    protected function _prepareLayout()
    {
        $helper = Mage::helper('consol_departments');

        $this->addTab('general_section', array(
            'label' => $helper->__('General Information'),
            'title' => $helper->__('General Information'),
            'content' => $this->getLayout()->createBlock('consol_departments/adminhtml_departments_edit_tabs_general')->toHtml(),
        ));

        return parent::_prepareLayout();
    }

}