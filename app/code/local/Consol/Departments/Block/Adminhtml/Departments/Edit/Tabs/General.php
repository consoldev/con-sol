<?php

class Consol_Departments_Block_Adminhtml_Departments_Edit_Tabs_General extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {

        $helper = Mage::helper('consol_departments');
        $model = Mage::registry('current_department');

        $storeId = Mage::app()->getRequest()->getParam('store');
        if(!$storeId){
            $storeId = Mage::app()->getDefaultStoreView()->getId();
        }
        $model['store_id'] = $storeId;

        $form = new Varien_Data_Form();
        $fieldset = $form->addFieldset('custom_form', array(
            'legend' => $helper->__('General Information')
        ));

        $fieldset->addField('name', 'text', array(
            'label' => $helper->__('Name'),
            'required' => true,
            'name' => 'name',
        ));
        $fieldset->addField('form_title', 'text', array(
            'label' => $helper->__('Form Title Text'),
            'required' => true,
            'name' => 'form_title',
        ));
        $fieldset->addField('store_id', 'hidden', array(
            'label' => $helper->__('store_id'),
            'name' => 'store_id',
        ));

        $fieldset->addField('phone', 'text', array(
            'label' => $helper->__('Phone'),
            'required' => false,
            'name' => 'phone',
        ));

        $fieldset->addField('email', 'text', array(
            'label' => $helper->__('Email'),
            'required' => false,
            'name' => 'email',
        ));

        $fieldset->addField('alias', 'text', array(
            'label' => $helper->__('Alias'),
            'required' => true,
            'name' => 'alias',
        ));

        $fieldset->addField('description', 'textarea', array(
            'label' => $helper->__('Description'),
            'required' => false,
            'name' => 'description',
        ));
        $fieldset->addField('meta_description', 'textarea', array(
            'label' => $helper->__('Meta Description'),
            'required' => false,
            'name' => 'meta_description',
        ));
        $fieldset->addField('meta_title', 'textarea', array(
            'label' => $helper->__('Meta Title'),
            'required' => false,
            'name' => 'meta_title',
        ));
        $fieldset->addField('meta_keywords', 'textarea', array(
            'label' => $helper->__('Meta Keywords'),
            'required' => false,
            'name' => 'meta_keywords',
        ));
        $fieldset->addField('order', 'text', array(
            'label' => $helper->__('Partner order'),
            'required' => true,
            'name' => 'order',
        ));

        $fieldset->addField('status', 'select', array(
            'label' => $helper->__('Status'),
            'required' => false,
            'name' => 'status',
            'values' =>array(
                array(
                    'value'     => 1,
                    'label'     => $helper->__('Enable'),
                ),
                array(
                    'value'     => 0,
                    'label'     => $helper->__('Disable'),
                )),
            'default' => '0',
        ));

        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }

}

