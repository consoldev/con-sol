<?php

class Consol_Departments_Block_Adminhtml_Departments_Grid extends Mage_Adminhtml_Block_Widget_Grid
{


    protected function _prepareCollection()
    {
        $storeId = Mage::app()->getRequest()->getParam('store');

        if(!$storeId){
            $storeId = Mage::app()->getDefaultStoreView()->getId();
        }
        if($storeId) {
            $collectionMain = Mage::getModel('consol_departments/departments')->getCollection()
                                                                              ->joinAdditional()
                                                                              ->addFieldToFilter('store_id', $storeId);
            $this->setCollection($collectionMain);
            return parent::_prepareCollection();
        }
    }

    protected function _prepareColumns()
    {

        $helper = Mage::helper('consol_departments');

        $this->addColumn('entity_id', array(
            'header' => $helper->__('Id'),
            'index' => 'entity_id'
        ));

        $this->addColumn('name', array(
            'header' => $helper->__('Name'),
            'index' => 'name'
        ));

        $this->addColumn('phone', array(
            'header' => $helper->__('Phone'),
            'index' => 'phone',
        ));

        $this->addColumn('alias', array(
            'header' => $helper->__('Alias'),
            'index' => 'alias'
        ));

        $this->addColumn('email', array(
            'header' => $helper->__('Email'),
            'index' => 'email',
        ));

        $this->addColumn('description', array(
            'header' => $helper->__('Description'),
            'index' => 'description',
        ));

        $this->addColumn('order', array(
            'header' => $helper->__('Partner order'),
            'index' => 'order',
        ));

        $this->addColumn('status', array(
            'header' => $helper->__('Status'),
            'index' => 'status',
            'frame_callback' => array($this, 'showStatus'),
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
        ));

        return $this;
    }

    public function getRowUrl($model)
    {
        return $this->getUrl('*/*/edit', array(
            'id' => $model->getEntityId(),
        ));
    }

    public function showStatus($status){
        if($status){
            switch($status) {
                case 0:
                    return 'Disable';
                    break;
                case 1:
                    return 'Enable';
                    break;

                default:
                    return 'Disable';
            }
        }else{
            return 'Disable';
        }

    }
}