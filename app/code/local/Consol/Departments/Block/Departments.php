<?php

class Consol_Departments_Block_Departments extends Mage_Core_Block_Template
{
    public function getDepartmentsCollection()
    {

        $storeId = Mage::app()->getStore()->getStoreId();

        $collectionMain = Mage::getModel('consol_departments/departments')->getCollection()
            ->joinAdditional()
            ->setOrder('main_table.order', 'asc')
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('store_id', $storeId);

        if($collectionMain->getSize() == 0){
            $defaultStoreId = Mage::app()
                ->getWebsite()
                ->getDefaultGroup()
                ->getDefaultStoreId();

            $collectionMain = Mage::getModel('consol_departments/departments')->getCollection()
                ->joinAdditional()
                ->setOrder('main_table.order', 'asc')
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('store_id', $defaultStoreId);
        }
        return $collectionMain;
    }



}

?>