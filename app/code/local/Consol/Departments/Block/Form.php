<?php

class Consol_Departments_Block_Form extends Mage_Core_Block_Template
{
    public function getDepartmentsCollection()
    {

        $storeId = Mage::app()->getStore()->getStoreId();

        $collectionMain = Mage::getModel('consol_departments/departments')->getCollection()
            ->joinAdditional()
            ->setOrder('main_table.order', 'asc')
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('store_id', $storeId);
        if(is_object($collectionMain) == false){
            $defaultStoreId = Mage::app()
                ->getWebsite()
                ->getDefaultGroup()
                ->getDefaultStoreId();

            $collectionMain = Mage::getModel('consol_departments/departments')->getCollection()
                ->joinAdditional()
                ->setOrder('main_table.order', 'asc')
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('store_id', $defaultStoreId);
        }
        return $collectionMain;
    }

    public function getDepartmentModel($alias){

        $storeId = Mage::app()->getStore()->getStoreId();

        $model = Mage::getModel('consol_departments/departments')->getCollection()
            ->joinAdditional()
            ->addFieldToFilter('alias', $alias)
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('store_id', $storeId)
            ->getFirstItem();
        if(is_object($model) == false){
            $defaultStoreId = Mage::app()
                ->getWebsite()
                ->getDefaultGroup()
                ->getDefaultStoreId();

            $model = Mage::getModel('consol_departments/departments')->getCollection()
                ->joinAdditional()
                ->addFieldToFilter('alias', $alias)
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('store_id', $defaultStoreId)
                ->getFirstItem();
        }
        return $model->getData();
    }

    public function getFirstItemModel(){

        $storeId = Mage::app()->getStore()->getStoreId();
        $model = Mage::getModel('consol_departments/departments')->getCollection()
            ->joinAdditional()
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('store_id', $storeId)
            ->getFirstItem();
        if(is_object($model) == false){
            $defaultStoreId = Mage::app()
                ->getWebsite()
                ->getDefaultGroup()
                ->getDefaultStoreId();

            $model = Mage::getModel('consol_departments/departments')->getCollection()
                ->joinAdditional()
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('store_id', $defaultStoreId)
                ->getFirstItem();
        }
        return $model->getData();
    }
    public function setHeaderTitle(){

    }
}

?>