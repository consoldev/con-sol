<?php

class Consol_Departments_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getJsonDepartments(){
        $storeId = Mage::app()->getStore()->getStoreId();
        $data =array();
        $collectionMain = Mage::getModel('consol_departments/departments')->getCollection()
            ->joinAdditional()
            ->setOrder('main_table.order', 'asc')
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('store_id', $storeId);
        if(is_object($collectionMain) == false){
            $defaultStoreId = Mage::app()
                ->getWebsite()
                ->getDefaultGroup()
                ->getDefaultStoreId();
            $collectionMain =Mage::getModel('consol_departments/departments')->getCollection()
                ->joinAdditional()
                ->setOrder('main_table.order', 'asc')
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('store_id', $defaultStoreId);
        }
        foreach ($collectionMain as $item){
            $data[$item->getEntityId()]['id'] = $item->getEntityId();
            $data[$item->getEntityId()]['name'] = $item->getName();
            $data[$item->getEntityId()]['phone'] = $item->getPhone();
            $data[$item->getEntityId()]['alias'] = $item->getAlias();
            $data[$item->getEntityId()]['description'] = $item->getDescription();
            $data[$item->getEntityId()]['email'] = $item->getEmail();
            $data[$item->getEntityId()]['form_title'] = $item->getFormTitle();
        }

        return json_encode($data);
    }


    public function getEmail($alias)
    {
        $collection = Mage::getModel('consol_departments/departments')->load($alias, 'alias');
        return $collection;
    }

    public function getDepartmentMeta($id){
        $storeId = Mage::app()->getStore()->getStoreId();
        $collectionMain = Mage::getModel('consol_departments/departments')->getCollection()
            ->joinAdditional()
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('store_id', $storeId)
            ->addFieldToFilter('alias', $id)
            ->getFirstItem();
        return $collectionMain;
    }

    public function getContact($id)
    {
        $collection = Mage::getModel('consol_departments/departments')->load($id, 'entity_id');
        return $collection;
    }

}

?>