<?php

class Consol_Departments_Model_Departments extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('consol_departments/departments');
    }

}