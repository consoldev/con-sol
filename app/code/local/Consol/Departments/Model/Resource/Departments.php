<?php

class Consol_Departments_Model_Resource_Departments extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('consol_departments/departments', 'entity_id');
    }
}