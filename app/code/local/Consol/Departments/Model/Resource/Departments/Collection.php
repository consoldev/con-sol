<?php

class Consol_Departments_Model_Resource_Departments_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('consol_departments/departments');
    }


    public function joinAdditional()
    {
        $this->join(
            array('info' => 'consol_departments/info'),
            'main_table.entity_id = info.entity_id',
            array('name', 'store_id', 'description', 'meta_title', 'meta_description', 'meta_keywords', 'form_title')
        );
        return $this;
    }
}

