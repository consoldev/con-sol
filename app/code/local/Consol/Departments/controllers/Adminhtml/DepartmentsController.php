<?php

class Consol_Departments_Adminhtml_DepartmentsController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        $this->loadLayout()->_setActiveMenu('departments');
        $this->_addContent($this->getLayout()->createBlock('consol_departments/adminhtml_departments'));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $storeId = Mage::app()->getRequest()->getParam('store');
        if(!$storeId){
            $storeId = Mage::app()->getDefaultStoreView()->getId();
        }
        $id = (int) $this->getRequest()->getParam('id');

        $modelMain = Mage::getModel('consol_departments/departments')
            ->getCollection()
            ->addFieldToFilter('entity_id', $id)
            ->getFirstItem();

        $modelInfo = Mage::getModel('consol_departments/info')
            ->getCollection()
            ->addFieldToFilter('entity_id', $id)
            ->addFieldToFilter('store_id', $storeId)
            ->getFirstItem();
        $modelMain->setName($modelInfo->getName());
        $modelMain->setDescription($modelInfo->getDescription());
        $modelMain->setMetaDescription($modelInfo->getMetaDescription());
        $modelMain->setMetaKeywords($modelInfo->getMetaKeywords());
        $modelMain->setMetaTitle($modelInfo->getMetaTitle());
        $modelMain->setFormTitle($modelInfo->getFormTitle());
        Mage::register('current_department', $modelMain);
        $this->loadLayout()->_setActiveMenu('departments');
        $this->_addLeft($this->getLayout()->createBlock('consol_departments/adminhtml_departments_edit_tabs'));
        $this->_addContent($this->getLayout()->createBlock('consol_departments/adminhtml_departments_edit'));

        $this->renderLayout();
    }

    public function saveAction()
    {

        if ($data = $this->getRequest()->getPost()) {
            try {
                $helper = Mage::helper('consol_partners');
                $modelMain = Mage::getModel('consol_departments/departments');
                $modelInfo = Mage::getModel('consol_departments/info');
                $id = $this->getRequest()->getParam('id');
                $main = $modelMain->load($id);
                $mainId = $main->getEntityId();
                if (isset($mainId)) {
                    $id = $main->getEntityId();

                    $main->setData($data)->setEntityId($id);
                    $main->save();

                    $data['entity_id'] = $main->getEntityId();
                    $info = $modelInfo
                        ->getCollection()
                        ->addFieldToFilter('entity_id', $id)
                        ->addFieldToFilter('store_id', $data['store_id'])
                        ->getFirstItem();

                    if($info->getId()){
                        $store = $info->getStoreId();
                        $infoId = $info->getId();
                        if($infoId){
                            if(!($data['store_id'])) {
                                $data['store_id'] = Mage::app()->getDefaultStoreView()->getId();
                            }
                            if ($store == $data['store_id']) {
                                $info->setData($data)->setId($infoId);

                                $info->save();
                            }else{
                                $info->setData($data);
                                $info->save();
                            }
                        }
                    } else {
                        $info->setData($data);
                        $info->save();
                    }

                } else {
                    $modelMain->setData($data);
                    $modelMain->save();
                    $data['store_id'] = Mage::app()->getDefaultStoreView()->getId();
                    $data['entity_id'] = $modelMain->getEntityId();
                    $modelInfo->setData($data);
                    $modelInfo->save();
                }

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Information was saved successfully'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array(
                    'id' => $this->getRequest()->getParam('id')
                ));
            }
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $helper = Mage::helper('consol_departments');
                $modelInfo = Mage::getModel('consol_departments/info')->getCollection()
                    ->addFieldToFilter('entity_id', $id);
                foreach($modelInfo as $model){
                    $model->delete();
                }

                $checkMore = Mage::getModel('consol_departments/info')->load($id, 'entity_id');
                if(!$checkMore->getId()) {
                    $mainModel = Mage::getModel('consol_departments/departments')->load($id);
                    $mainModel->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Record was deleted successfully'));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $ids = $this->getRequest()->getParam('id', null);

        if (is_array($ids) && sizeof($ids) > 0) {

            try {
                $helper = Mage::helper('consol_departments');

                foreach ($ids as $id) {
                    $modelInfo = Mage::getModel('consol_departments/info')->getCollection()
                        ->addFieldToFilter('entity_id', $id);
                    foreach($modelInfo as $model){
                        $model->delete();
                    }
                    $mainModel = Mage::getModel('consol_departments/departments')->load($id);
                    $mainModel->delete();
                }

                $this->_getSession()->addSuccess($this->__('Total of %d records have been deleted', sizeof($ids)));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        } else {
            $this->_getSession()->addError($this->__('Please select records'));
        }
        $this->_redirect('*/*');
    }

}

?>