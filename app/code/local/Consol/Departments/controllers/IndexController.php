<?php

class Consol_Departments_IndexController extends Mage_Core_Controller_Front_Action
{
    const XML_PATH_EMAIL_SENDER = 'contacts/email/sender_email_identity';

    public function indexAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $pageTitle = Mage::getModel('cms/page')->load('contact_seo', 'identifier');

        $this->getLayout()->getBlock('head')->setTitle($pageTitle->getTitle());
        $this->getLayout()->getBlock('head')->setDescription($pageTitle->getMetaDescription());
        $this->getLayout()->getBlock('head')->setKeywords($pageTitle->getMetaKeywords());
        $this->renderLayout();
    }

    public function sendAction()
    {
        $this->loadLayout();
        $alias = $this->getRequest()->getParam('id');
        $depart = Mage::helper('consol_departments')->getDepartmentMeta($alias);
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');

        $this->getLayout()->getBlock('head')->setTitle($depart->getMetaTitle());
        $this->getLayout()->getBlock('head')->setDescription($depart->getMetaDescription());
        $this->getLayout()->getBlock('head')->setKeywords($depart->getMetaKeywords());
        $this->renderLayout();
    }

    public function postAction()
    {
        $referer = 'contact/send/'.$this->getRequest()->getParam('param');
        $post = $this->getRequest()->getPost();

        if ($post) {
            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);

            try {

                $postObject = new Varien_Object();
                $postObject->setData($post);
                $error = false;

                $recipient = Mage::helper('consol_departments')->getContact($post['id']);

                if (!$recipient) {
                    $error = true;
                }
                if (!Zend_Validate::is(trim($post['lastname']), 'NotEmpty')) {
                    $error = true;
                }
                if (!Zend_Validate::is(trim($post['firstname']), 'NotEmpty')) {
                    $error = true;
                }
                if (!Zend_Validate::is(trim($post['message']), 'NotEmpty')) {
                    $error = true;
                }
                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }
                $postObject->setFileNames($this->saveFiles());

                if ($error) {
                    throw new Exception('validation');
                }
                if (!$this->sendEmail($postObject, $recipient['email'])) {

                    throw new Exception('mail');
                }

                $translate->setTranslateInline(true);

                Mage::getSingleton('customer/session')->addSuccess(Mage::helper('consol_departments')->__('Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.'));
                $this->_redirect($referer);

                return;
            } catch (Exception $e) {
                $translate->setTranslateInline(true);

                Mage::getSingleton('customer/session')->addError(Mage::helper('consol_departments')->__('Unable to submit your request. Please, try again later'));
                $this->_redirect($referer);
                return;
            }
        } else {
            $this->_redirect($referer);
        }
    }

    public function saveFiles()
    {
        $postFileNames = $_POST['files-array'];
        $fileNames = array();

        if (isset($_FILES['replyfiles'])) {
            $i=0;
            $filesSize = null;
            foreach ($_FILES as $item) {
                foreach ($item['size'] as $filesize){
                    $filesSize += $filesize;
                }
            }
            if($filesSize/1000000 > 20){
                Mage::getSingleton('customer/session')->addError('Files size is too large!');
                return false;
            }
            try {
                foreach ($_FILES['replyfiles'] as $file) {

                    $fileName = $_FILES['replyfiles']['name'][$i];
                    if ($fileName AND in_array($fileName, $postFileNames)) {
                        $uploader = new Varien_File_Uploader(
                            array(
                                'name' => $_FILES['replyfiles']['name'][$i],
                                'type' => $_FILES['replyfiles']['type'][$i],
                                'tmp_name' => $_FILES['replyfiles']['tmp_name'][$i],
                                'error' => $_FILES['replyfiles']['error'][$i],
                                'size' => $_FILES['replyfiles']['size'][$i]
                            )
                        );

                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        $path = Mage::getBaseDir('var') . DS . 'tmp' . DS . 'departments';
                        if (!is_dir($path)) {
                            mkdir($path, 0775, true);
                        }
                        $uploader->save($path . DS, $fileName);
                        $fileNames[] = $path . DS . $fileName;
                    }
                    $i++;
                }
            } catch (Exception $e) {
                Mage::getSingleton('customer/session')->addError($e->getMessage());
                return false;
            }
        }

        return $fileNames;
    }

    public function sendEmail($postObj, $recipient)
    {
        $mailTemplate = Mage::getModel('core/email_template')->loadDefault('department_email_template');

        /* @var $mailTemplate Mage_Core_Model_Email_Template */
        $mailTemplate->setDesignConfig(array('area' => 'frontend',))
            ->setReplyTo($postObj->getEmail());

        foreach ($postObj->getFileNames() as $file) {
            $mailTemplate->getMail()
                ->createAttachment(
                    file_get_contents($file),
                    Zend_Mime::TYPE_OCTETSTREAM,
                    Zend_Mime::DISPOSITION_ATTACHMENT,
                    Zend_Mime::ENCODING_BASE64,
                    basename($file)
                );
        }
        $mailTemplate->sendTransactional(
            $mailTemplate,
            Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
            $recipient,
            null,
            array('data' => $postObj)
        );
        foreach ($postObj->getFileNames() as $file) {
            unlink($file);
        }
        return $mailTemplate->getSentSuccess();
    }

    public function compareAction(){
        $response = array();

        if ($productId = (int) $this->getRequest()->getParam('product')) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($productId);

            if ($product->getId()/* && !$product->isSuper()*/) {
                Mage::getSingleton('catalog/product_compare_list')->addProduct($product);
                $syshelper = Mage::helper("systemhelp");
                if($syshelper->checkIfProductInComparelist($product->getId())){
                    $response['status'] = 'SUCCESS';
                    $response['message'] = $this->__('The product %s has been added to comparison list.', Mage::helper('core')->escapeHtml($product->getName()));
                    Mage::register('referrer_url', $this->_getRefererUrl());
                    Mage::helper('catalog/product_compare')->calculate();
                    Mage::dispatchEvent('catalog_product_compare_add_product', array('product' => $product));
                    $this->loadLayout();
                    $sidebar_block = $this->getLayout()->getBlock('catalog.compare.sidebar');
                    $sidebar_block->setTemplate('page/html/compare-ajax.phtml');
                    $sidebar = $sidebar_block->toHtml();
                    $response['header_compare'] = $sidebar;
                }
                else{
                    $response['status'] = 'ERROR';
                    $response['message'] = $this->__('Can not add product to compare list');
                }
            }
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        return;
    }

}

?>