<?php

$installer = $this;
$table = $installer->getTable('consol_departments/departments');

$installer->startSetup();

$installer->getConnection()->addColumn($table, 'alias', array(
    'comment'   => 'Alias',
    'nullable'  => false,
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
));

$installer->endSetup();