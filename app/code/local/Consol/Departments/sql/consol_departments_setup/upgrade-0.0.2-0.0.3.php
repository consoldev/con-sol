<?php

$content = '
<span class="page-subtitle">Контакты, адреса отделов компании</span>
<div class="contacts-requisites clearfix">
    <div class="block-title"><span>Реквизиты компании</span></div>
    <dl class="req-dict col col-xs-12">
        <dt class="col col-md-6 col-xs-12"><div>Название компании</div></dt>
        <dd class="col col-md-6 col-xs-12"><div>Товариство з обмеженою відповідальністю «Тарне Рішення»</div></dd>
        <dt class="col col-md-6 col-xs-12"><div>Юридический и почтовый адрес</div></dt>
        <dd class="col col-md-6 col-xs-12"><div><span>02081, Украина, г. Киев,</span>
            <span>ул. Здолбуновская, 7а</span></div></dd>
        <dt class="col col-md-6 col-xs-12"><div>ЕГРПОУ</div></dt>
        <dd class="col col-md-6 col-xs-12"><div>38193528</div></dd>
        <dt class="col col-md-6 col-xs-12"><div>ИНН</div></dt>
        <dd class="col col-md-6 col-xs-12"><div>381935226516</div></dd>
        <dt class="col col-md-6 col-xs-12"><div>№ св-ва НДС</div></dt>
        <dd class="col col-md-6 col-xs-12"><div>200056762</div></dd>
        <dt class="col col-md-6 col-xs-12"><div>Банковские реквизиты</div></dt>
        <dd class="col col-md-6 col-xs-12"><div><span>р/р 26008210256105</span>
            <span>в АТ «ПроКредит Банк» м.Київ</span></div></dd>
        <dt class="col col-md-6 col-xs-12"><div>МФО</div></dt>
        <dd class="col col-md-6 col-xs-12"><div>320984</div></dd>
        <dt class="col col-md-6 col-xs-12"><div>Факс</div></dt>
        <dd class="col col-md-6 col-xs-12"><div><a class="tel-link" href="tel:+380444951004">+38-044-503-01-24</a></div></dd>
    </dl>
</div>
';
//if you want one block for each store view, get the store collection
$stores = Mage::getModel('core/store')->getCollection()->addFieldToFilter('store_id', array('gt'=>0))->getAllIds();
//if you want one general block for all the store viwes, uncomment the line below
//$stores = array(0);
$loader = Mage::getModel('cms/block')->load('contact_propertries_block');
//if(!$loader) {
    foreach ($stores as $store) {
        $block = Mage::getModel('cms/block');
        $block->setTitle('Contact properties block');
        $block->setIdentifier('contact_propertries_block');
        $block->setStores(array($store));
        $block->setIsActive(1);
        $block->setContent($content);
        $block->save();
        //   }
    }
?>