<?php

$content = '<div class="contacts-schedule clearfix">
                <div class="block-title"><span>График работы</span></div>
                <div class="block-content">
                    <dl class="col col-xs-12 col-md-6">
                        <dt><div>Пн-Пт</div></dt>
                        <dd><div>с 9.00 до 18.00</div></dd>
                    </dl>
                    <dl class="col col-xs-12 col-md-6">
                        <dt><div>Сб-Вс</div></dt>
                        <dd><div>выходные</div></dd>
                    </dl>
                </div>
            </div>
            <div class="contacts-map clearfix">
                <div class="block-title"><span>Схема проезда</span></div>
                <div id="map" style="width: 100%; height: 450px">
               
                </div>
            </div>
            
              <div class="product-slider-outer">
                    <div class="block-title"><span>Въезд на территорию ТОВ "Тарне Рішення"</span></div>
                    <ul id="owl-demo" class="owl-carousel product-slider page-contact-slider">
                        <li class="item">
                                   <img src="https://con-sol.com.ua/skin/frontend/sm_market/default/images/contacts-photopg-5.jpg">
                        </li>
                        <li class="item">
                                   <img src="https://con-sol.com.ua/skin/frontend/sm_market/default/images/contacts-photopg-5.jpg">
                        </li>
                        <li class="item">
                                   <img src="https://con-sol.com.ua/skin/frontend/sm_market/default/images/contacts-photopg-5.jpg">
                        </li>
                        <li class="item">
                                   <img src="https://con-sol.com.ua/skin/frontend/sm_market/default/images/contacts-photopg-5.jpg">
                        </li>
                    </ul>
                </div>
';
//if you want one block for each store view, get the store collection
$stores = Mage::getModel('core/store')->getCollection()->addFieldToFilter('store_id', array('gt'=>0))->getAllIds();
//if you want one general block for all the store viwes, uncomment the line below
//$stores = array(0);
$loader = Mage::getModel('cms/block')->load('contact_map_block');
//if(!$loader) {
    foreach ($stores as $store) {
        $block = Mage::getModel('cms/block');
        $block->setTitle('Contact map block');
        $block->setIdentifier('contact_map_block');
        $block->setStores(array($store));
        $block->setIsActive(1);
        $block->setContent($content);
        $block->save();
        //   }
    }
?>