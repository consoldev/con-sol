<?php

$installer = $this;
$table = $installer->getTable('consol_departments/info');

$installer->startSetup();

$installer->getConnection()->addColumn($table, 'meta_description', array(
    'comment'   => 'Meta Description',
    'nullable'  => false,
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
));
$installer->getConnection()->addColumn($table, 'meta_keywords', array(
    'comment'   => 'Meta Keywords',
    'nullable'  => false,
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
));
$installer->getConnection()->addColumn($table, 'meta_title', array(
    'comment'   => 'Meta Title',
    'nullable'  => false,
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
));
$installer->endSetup();