<?php

$installer = $this;
$table = $installer->getTable('consol_departments/info');

$installer->startSetup();

$installer->getConnection()->addColumn($table, 'form_title', array(
    'comment'   => 'Form Title text',
    'nullable'  => false,
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
));

$installer->endSetup();