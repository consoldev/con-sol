<?php

class Consol_Partners_Block_Adminhtml_Partners extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    protected function _construct()
    {
        parent::_construct();
        $helper = Mage::helper('consol_partners');
        $this->_blockGroup = 'consol_partners';
        $this->_controller = 'adminhtml_partners';

        $this->_headerText = $helper->__('Partners Management');

        $this->_addButtonLabel = $helper->__('Add Partners');
    }
}

?>