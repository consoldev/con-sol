<?php

class Consol_Partners_Block_Adminhtml_Partners_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    protected function _construct()
    {
        $this->_blockGroup = 'consol_partners';
        $this->_controller = 'adminhtml_partners';
    }

    public function getHeaderText()
    {
        $helper = Mage::helper('consol_partners');
        $model = Mage::registry('current_partner_main');

        if ($model->getId()) {
            return $helper->__("Edit partner info", $this->escapeHtml($model->getName()));
        } else {
            return $helper->__("Add partner item");
        }
    }

}