<?php

class Consol_Partners_Block_Adminhtml_Partners_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        $helper = Mage::helper('consol_partners');

        parent::__construct();
        $this->setId('partner_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($helper->__('Partner Information'));
    }

    protected function _prepareLayout()
    {
        $helper = Mage::helper('consol_partners');

        $this->addTab('general_section', array(
            'label' => $helper->__('General Information'),
            'title' => $helper->__('General Information'),
            'content' => $this->getLayout()->createBlock('consol_partners/adminhtml_partners_edit_tabs_general')->toHtml(),
        ));

        /*$this->addTab('custom_section', array(
            'label' => $helper->__('Custom Fields'),
            'title' => $helper->__('Custom Fields'),
            'content' => $this->getLayout()->createBlock('consol_partners/adminhtml_partners_edit_tabs_custom')->toHtml(),
        ));*/
        return parent::_prepareLayout();
    }

}