<?php

class Consol_Partners_Block_Adminhtml_Partners_Edit_Tabs_Custom extends Mage_Adminhtml_Block_Widget_Form
{
      protected function _prepareForm()
    {
        $helper = Mage::helper('consol_partners');
        $model = Mage::registry('current_partner_info');

        $form = new Varien_Data_Form();
        $fieldset = $form->addFieldset('general_form', array(
            'legend' => $helper->__('Additional Information')
        ));

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

}