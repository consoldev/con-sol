<?php

class Consol_Partners_Block_Adminhtml_Partners_Edit_Tabs_General extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {

        $helper = Mage::helper('consol_partners');
        $model = Mage::registry('current_partner_main');
       // $modelInfo = Mage::registry('current_partner_info');
        $form = new Varien_Data_Form();
        $fieldset = $form->addFieldset('custom_form', array(
            'legend' => $helper->__('General Information')
        ));

        $fieldset->addField('store_id', 'hidden', array(
            'label' => $helper->__('store_id'),
            'name' => 'store_id',
        ));

        $fieldset->addField('name', 'text', array(
            'label' => $helper->__('Name'),
            'required' => true,
            'name' => 'name',
        ));

        $fieldset->addField('description', 'editor', array(
            'name' => 'description',
            'label' => $helper->__('Description'),
            'style' => 'height:12em;width:500px;',
            'wysiwyg' => true,
            'required' => true,
            'config' => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
        ));

        $fieldset->addField('link', 'text', array(
            'label' => $helper->__('Link'),
            'required' => true,
            'name' => 'link',
        ));

        $fieldset->addField('logo', 'image', array(
            'label' => $helper->__('Logo'),
            'required' => false,
            'name' => 'logo',
            'after_element_html' => $this->getImageHtml('logo', $model->getLogo())
        ));
        $fieldset->addField('order', 'text', array(
            'label' => $helper->__('Partner order'),
            'required' => true,
            'name' => 'order',
        ));
        $fieldset->addField('status', 'select', array(
            'label' => $helper->__('Status'),
            'required' => false,
            'name' => 'status',
            'values' =>array(
                array(
                    'value'     => 1,
                    'label'     => $helper->__('Enable'),
                ),
                array(
                    'value'     => 0,
                    'label'     => $helper->__('Disable'),
                )),
            'default' => '0',
        ));

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    protected function getImageHtml($field, $img)
    {
        $html = '';
        if ($img) {
            $html .= '<p style="margin-top: 5px">';
            $html .= '<img style="max-width:300px" src="' . Mage::getBaseUrl('media') . 'partners'. DS . $img . '">';
            $html .= '<input type="hidden" value="' . $img . '" name="old_' . $field . '"/>';
            $html .= '</p>';
        }

        return $html;
    }

}

