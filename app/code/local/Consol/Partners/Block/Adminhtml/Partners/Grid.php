<?php

class Consol_Partners_Block_Adminhtml_Partners_Grid extends Mage_Adminhtml_Block_Widget_Grid
{


    protected function _prepareCollection()
    {
        $storeId = Mage::app()->getRequest()->getParam('store');
        if(!$storeId){
            $storeId = Mage::app()->getDefaultStoreView()->getId();
        }

        if($storeId) {
            $collectionMain = Mage::getModel('consol_partners/info')->getCollection()
                                                                              ->joinAdditional()
                                                                              ->addFieldToFilter('store_id', $storeId);
            $this->setCollection($collectionMain);
            return parent::_prepareCollection();
        }else{
            $collectionMain = Mage::getModel('consol_partners/info')->getCollection()
                                                                              ->joinAdditional();
            $this->setCollection($collectionMain);

            return parent::_prepareCollection();
        }
    }

    protected function _prepareColumns()
    {

        $helper = Mage::helper('consol_partners');

        $this->addColumn('entity_id', array(
            'header' => $helper->__('Id'),
            'index' => 'entity_id'
        ));

        $this->addColumn('name', array(
            'header' => $helper->__('Name'),
            'index' => 'name'
        ));

        $this->addColumn('description', array(
            'header' => $helper->__('Description'),
            'index' => 'description'
        ));

        $this->addColumn('link', array(
            'header' => $helper->__('Link'),
            'index' => 'link',
        ));

        $this->addColumn('logo', array(
            'header' => $helper->__('Logo'),
            'index' => 'logo',
            'filter' => false,
            'frame_callback' => array($this, 'callback_image'),
        ));

        /*$this->addColumn('store_id', array(
            'header' => $helper->__('Store'),
            'index' => 'store_id',
        ));*/

        $this->addColumn('order', array(
            'header' => $helper->__('Partner order'),
            'index' => 'order',
        ));

        $this->addColumn('status', array(
            'header' => $helper->__('Status'),
            'index' => 'status',
            'frame_callback' => array($this, 'showStatus'),
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
        ));

        return $this;
    }

    public function getRowUrl($model)
    {
        return $this->getUrl('*/*/edit', array(
            'id' => $model->getEntityId(),
        ));
    }

    public function callback_image($value)
    {
        $width = 70;
        $height = 70;
        if($value) {
            return "<img src='" . Mage::getBaseUrl('media') . 'partners' . DS . $value . "' width=" . $width . " height=" . $height . "/>";
        }
    }

    public function showStatus($status){
        if($status){
            switch($status) {
                case 0:
                    return 'Disable';
                    break;
                case 1:
                    return 'Enable';
                    break;

                default:
                    return 'Disable';
            }
        }else{
            return 'Disable';
        }

    }
}