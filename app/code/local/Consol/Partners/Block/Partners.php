<?php

class Consol_Partners_Block_Partners extends Mage_Core_Block_Template
{
    public function getPartnersCollection(){

        $storeId = Mage::app()->getStore()->getStoreId();
        $collectionMain = Mage::getModel('consol_partners/info')->getCollection()
            ->joinAdditional()
            ->setOrder('main.order', 'asc')
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('store_id', $storeId);
        if($collectionMain->getSize() > 0) {
            return $collectionMain;
        }
        else{
            $defaultStoreId = Mage::app()->getDefaultStoreView()->getId();
            $collectionMain = Mage::getModel('consol_partners/info')->getCollection()
                ->joinAdditional()
                ->setOrder('main.order', 'asc')
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('store_id', $defaultStoreId);
            return $collectionMain;
        }
    }

}

?>