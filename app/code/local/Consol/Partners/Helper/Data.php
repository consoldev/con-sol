<?php

class Consol_Partners_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function deletePartnersImage($model)
    {
        $path = Mage::getBaseDir('media') . DS . 'partners' . DS;
        $name = $model->getLogo();
        if(file_exists($path.$name)){
            unlink($model->getEntityId . $path . $name);
        }

    }

    public function getPartnersImage($model)
    {
        $name = $model->getLogo();

        $url = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA).DS.'partners'.DS;
        //var_dump($url.$name);
        if(file_exists($url.$name)){
            return $url.$name;
        }
    }
}

?>