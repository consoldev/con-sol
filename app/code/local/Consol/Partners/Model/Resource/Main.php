<?php

class Consol_Partners_Model_Resource_Main extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('consol_partners/main', 'entity_id');
    }
}