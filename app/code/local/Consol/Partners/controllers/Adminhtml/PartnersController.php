<?php

class Consol_Partners_Adminhtml_PartnersController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        $this->loadLayout()->_setActiveMenu('partners');
        $this->_addContent($this->getLayout()->createBlock('consol_partners/adminhtml_partners'));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $storeId = Mage::app()->getRequest()->getParam('store');
        if(!$storeId){
            $storeId = Mage::app()->getDefaultStoreView()->getId();
        }
        $id = (int) $this->getRequest()->getParam('id');
        Mage::getSingleton('core/session')->setStoreId($storeId);

        $modelInfo = Mage::getModel('consol_partners/info')
            ->getCollection()
            ->addFieldToFilter('entity_id', $id)
            ->addFieldToFilter('store_id', $storeId)
            ->getFirstItem();
        $modelMain = Mage::getModel('consol_partners/main');

        if($modelInfo && $modelMain) {
            if($id) {
                $modelMain->load($id, 'entity_id');
                $modelMain->setName($modelInfo->getName());
                $modelMain->setDescription($modelInfo->getDescription());
                $modelMain->setStoreId($storeId);
            }
            Mage::register('current_partner_main', $modelMain);
        }
        $this->loadLayout()->_setActiveMenu('partners');
        $this->_addLeft($this->getLayout()->createBlock('consol_partners/adminhtml_partners_edit_tabs'));
        $this->_addContent($this->getLayout()->createBlock('consol_partners/adminhtml_partners_edit'));

        $this->renderLayout();
    }

    public function saveAction()
    {   

        if ($data = $this->getRequest()->getPost()) {
            try {
                $helper = Mage::helper('consol_partners');
                $modelMain = Mage::getModel('consol_partners/main');
                $modelInfo = Mage::getModel('consol_partners/info');
                $id = $this->getRequest()->getParam('id');
                $main = $modelMain->load($id);
                $mainId = $main->getEntityId();
                if (isset($mainId)) {
                    $id = $main->getEntityId();
                    if (isset($data['logo'])) {
                        if(isset($data['logo']['delete'])){
                            if ($helper->getPartnersImage($modelMain)) {
                                $helper->deletePartnersImage($modelMain);
                                $data['logo']['value'] = '';
                            }
                        }
                        $image = $data['logo']['value'];
                        $data['logo'] = $image;
                    }

                    $main->setData($data)->setEntityId($id);
                    $main->save();

                    $data['entity_id'] = $main->getEntityId();
                    $info = $modelInfo
                        ->getCollection()
                        ->addFieldToFilter('entity_id', $id)
                        ->addFieldToFilter('store_id', $data['store_id'])
                        ->getFirstItem();

                    if($info->getId()){
                        $store = $info->getStoreId();
                        $infoId = $info->getId();
                        if($infoId){
                            if(!($data['store_id'])) {
                                $data['store_id'] = Mage::app()->getDefaultStoreView()->getId();
                            }
                            if ($store == $data['store_id']) {
                                $info->setData($data)->setId($infoId);

                                $info->save();
                            }else{
                                $info->setData($data);
                                $info->save();
                            }
                        }
                    } else {
                        $info->setData($data);
                        $info->save();
                    }

                } else {
                    $modelMain->setData($data);
                    $modelMain->save();
                    $data['store_id'] = Mage::app()->getDefaultStoreView()->getId();
                    $data['entity_id'] = $modelMain->getEntityId();
                    $modelInfo->setData($data);
                    $modelInfo->save();
                }

                if (isset($_FILES['logo']['name']) && $_FILES['logo']['name'] != '') {
                    try {
                        $helper = Mage::helper('consol_partners');
                        $uploader = new Varien_File_Uploader('logo');
                        $name = explode('.', $_FILES['logo']['name']);
                        $allowedExtentions = array('jpg', 'jpeg', 'gif', 'png', 'JPG', 'JPEG', 'GIF', 'PNG');

                        if(!in_array($name[1], $allowedExtentions)){
                            Mage::getSingleton('adminhtml/session')->addError($this->__('You choose image with wrong extension. Allowed extensions: jpg, jpeg, gif, png'));

                            $this->_redirect('*/*/edit', array(
                                'id' => $this->getRequest()->getParam('id')));
                            return;
                        }

                        $uploader->setAllowedExtensions($allowedExtentions);
                        $uploader->setAllowRenameFiles(false);

                        $uploader->setFilesDispersion(false);

                        $path = Mage::getBaseDir('media') . DS . 'partners' . DS;
                        preg_match('/\.([^\.]+)$/', $_FILES['logo']['name'], $extension);

                        if ($modelMain->getLogo()) {
                            if ($helper->getPartnersImage($modelMain)) {

                                $helper->deletePartnersImage($modelMain);
                            }
                        }
                        $uploader->save($path, $modelMain->getEntityId() . $name[0] . strtolower($extension[0]));
                        $modelMain->setLogo($uploader->getUploadedFileName());

                        $modelMain->save();

                    } catch (Exception $e) {
                    }

                }

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Information was saved successfully'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array(
                    'id' => $this->getRequest()->getParam('id')
                ));
            }
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            $storeId = Mage::getSingleton('core/session')->getStoreId();
            try {
                $helper = Mage::helper('consol_partners');
                $modelInfo = Mage::getModel('consol_partners/info')
                    ->getCollection()
                    ->addFieldToFilter('entity_id', $id)
                    ->addFieldToFilter('store_id', $storeId)
                    ->getFirstItem();
                $modelInfo->delete();
                $checkMore = Mage::getModel('consol_partners/info')->load($id, 'entity_id');
                if(!$checkMore->getId()) {
                    $mainModel = Mage::getModel('consol_partners/main')->load($id);
                    if ($mainModel->getLogo() && $helper->getPartnersImage($mainModel)) {
                        $helper->deletePartnersImage($mainModel);
                    }
                    $mainModel->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Record was deleted successfully'));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $partnersId = $this->getRequest()->getParam('id', null);

        if (is_array($partnersId) && sizeof($partnersId) > 0) {

            try {
                $helper = Mage::helper('consol_partners');

                foreach ($partnersId as $id) {
                    $modelInfo = Mage::getModel('consol_partners/info')->load($id, 'id');
                    $deletedInfo[]= $modelInfo->getEntityId();
                    $modelInfo->delete();
                }

                $uniqueDeletedIds = array_unique($deletedInfo);

                foreach ($uniqueDeletedIds as $id) {
                    $modelInfo = Mage::getModel('consol_partners/info')->load($id, 'entity_id');
                    if(!$modelInfo->getId()) {
                        $mainModel = Mage::getModel('consol_partners/main')->load($id);

                        if ($mainModel->getLogo()) {
                            if ($helper->getPartnersImage($mainModel)) {

                                $helper->deletePartnersImage($mainModel);
                            }
                        }
                        $mainModel->delete();
                    }
                }

                $this->_getSession()->addSuccess($this->__('Total of %d records have been deleted', sizeof($partnersId)));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        } else {
            $this->_getSession()->addError($this->__('Please select records'));
        }
        $this->_redirect('*/*');
    }

}

?>