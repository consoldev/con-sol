<?php

class Consol_Systemhelp_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function checkIfProductInWishlist($productId)
    {
        if ($productId AND Mage::getSingleton('customer/session')->isLoggedIn()) {

            $customer = Mage::getSingleton('customer/session')->getCustomer();

            $wishlist = Mage::getModel('wishlist/wishlist')->loadByCustomer($customer, true);
            $wishlistCollection = $wishlist->getItemCollection();
            $wishlistCount = $wishlist->getItemsCount();

            if ($wishlistCount > 0) {
                foreach ($wishlistCollection as $_item) {
                    if ($_item->getProductId() == $productId) {
                        return true;
                    }
                }
            } else {
                return false;
            }
        }
    }



    public function checkIfProductInComparelist($productId){
        if($productId) {
            $collection = $_helper = Mage::helper('catalog/product_compare')->getItemCollection();
            if($collection->count() > 0)
            foreach($collection as $product) {
                if($product->getEntityId() == $productId){
                    return true;
                }
            }
        }
    }

    public function getCategorieIds(){
        $_id = 2; // category 0
        return $catIds = Mage::getModel('catalog/category')
            ->getCollection()
            ->addFieldToFilter('parent_id', array('eq'=>$_id))
            ->addAttributeToFilter('is_active', 1)
            ->addAttributeToSort('position', 'asc');
    }
}

?>