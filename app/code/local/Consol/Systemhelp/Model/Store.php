<?php

class Consol_Systemhelp_Model_Store extends Mage_Core_Model_Store
{
    public function formatPrice($price, $includeContainer = true)
    {
        if ($this->getCurrentCurrency()) {
            $currency_code = Mage::app()->getStore()->getCurrentCurrencyCode();
            $currency_symbol = Mage::app()->getLocale()->currency( $currency_code )->getSymbol();
            $price = $this->getCurrentCurrency()->format($price, array('precision' => '0'), $includeContainer);
            $price = str_replace($currency_symbol, '<span class="symbol">'.$currency_symbol.'</span>', $price);
            return $price;
        }
        return $price;
    }
}