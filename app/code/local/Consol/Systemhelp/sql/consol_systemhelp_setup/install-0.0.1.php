<?php

$content = ' <div class="advantages">
               <div class="advantages-left">
                 <ul>
                    <li class="li-top">Более 5 лет на рынке</li>
                    <li>Надёжность и профессионализм</li>
                 </ul>
               </div>
               <div class="advantages-icon">
                 <a href="https://con-sol.com.ua/partners/" class="icon-tp" title="Нам доверяют">
                    <i></i>
                 </a>
               </div>
               <div class="advantages-right">
                   <ul>
                     <li class="li-top">Более 100 постоянных клиентов</li>
                     <li>Широкий выбор продукции</li>
                   </ul>
               </div>
             </div>
';
//if you want one block for each store view, get the store collection
$stores = Mage::getModel('core/store')->getCollection()->addFieldToFilter('store_id', array('gt'=>0))->getAllIds();
//if you want one general block for all the store viwes, uncomment the line below
//$stores = array(0);
foreach ($stores as $store){
    $block = Mage::getModel('cms/block');
    $block->setTitle('Header Partners Logo');
    $block->setIdentifier('header_partners_logo');
    $block->setStores(array($store));
    $block->setIsActive(1);
    $block->setContent($content);
    $block->save();
}

?>