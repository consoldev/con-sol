<?php

$entityTypeId = Mage::getModel('catalog/product')->getResource()->getTypeId();

$cloneSetId = 4;

$model =Mage::getModel('eav/entity_attribute_set')
    ->getCollection()
    ->setEntityTypeFilter($entityTypeId)
    ->addFieldToFilter('attribute_set_name', 'Barrels')
    ->getFirstItem();
if(!is_object($model)){
    $model = Mage::getModel('eav/entity_attribute_set');
}
if(!is_numeric($model->getAttributeSetId())){
    $new = true;
}
$model->setEntityTypeId($entityTypeId);

$model->setAttributeSetName('Barrels');
$model->validate();
$model->save();

if($new){
    $model->initFromSkeleton($cloneSetId)->save();
}


$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
if(!$installer->getAttributeId('catalog_product', 'barrel_condition')) {
    $installer->addAttribute('catalog_product', 'barrel_condition', array(
        'attribute_set' => 'Barrels',
        'label' => 'Состояние бочки',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Новые',
                1 => 'Б/у',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'barrel_condition');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Barrels');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'product_nomination_barrels')) {
    $installer->addAttribute('catalog_product', 'product_nomination_barrels', array(
        'attribute_set' => 'Barrels',
        'label' => 'Наименование товара бочки',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Бочка металлическая',
                1 => 'Бочка пластиковая',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'product_nomination_barrels');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Barrels');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'barrel_type')) {
    $installer->addAttribute('catalog_product', 'barrel_type', array(
        'attribute_set' => 'Barrels',
        'label' => 'Вид бочек',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Бочка с закрытым верхом',
                1 => 'Бочка с открывающимся верхом',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'barrel_type');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Barrels');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'barrel_form')) {
    $installer->addAttribute('catalog_product', 'barrel_form', array(
        'attribute_set' => 'Barrels',
        'label' => 'Форма бочки',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Бочка цилиндрическая',
                1 => 'Бочка конусная',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'barrel_form');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Barrels');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'barrel_capacity')) {
    $installer->addAttribute('catalog_product', 'barrel_capacity', array(
        'attribute_set' => 'Barrels',
        'label' => 'Обьем бочки',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => '227',
                1 => '217',
                2 => '216,5',
                3 => '213',
                4 => '210',
                5 => '127',
                6 => '100',
                7 => '65',
                8 => '60',
                9 => '55',
                10 => '50',
                11 => '48',
                12 => '45',
                13 => '32',
                14 => '30',
                15 => '20',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'barrel_capacity');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Barrels');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'application_area_barrels')) {
    $installer->addAttribute('catalog_product', 'application_area_barrels', array(
        'attribute_set' => 'Barrels',
        'label' => 'Область применения бочки',
        'input' => 'multiselect',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Химические продукты жидкие',
                1 => 'Химические продукты пастообразные',
                2 => 'Нефтехимические вещества жидкие',
                3 => 'Нефтехимические вещества пастообразные',
                4 => 'Опасные грузы жидкие',
                5 => 'Опасные грузы пастообразные',
                6 => 'Пищевые продукты жидкие',
                7 => 'Пищевые продукты пастообразные',
                8 => 'Пищевые продукты сыпучие',
                9 => 'Технические продукты жидкие',
                10 => 'Технические продукты пастообразные',
                11 => 'Технические продукты сыпучие',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'application_area_barrels');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Barrels');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

$installer->endSetup();
?>