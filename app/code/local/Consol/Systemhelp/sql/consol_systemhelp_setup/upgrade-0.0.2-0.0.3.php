<?php

$entityTypeId = Mage::getModel('catalog/product')->getResource()->getTypeId();

$cloneSetId = 4;

$model =Mage::getModel('eav/entity_attribute_set')
    ->getCollection()
    ->setEntityTypeFilter($entityTypeId)
    ->addFieldToFilter('attribute_set_name', 'Cans')
    ->getFirstItem();
if(!is_object($model)){
    $model = Mage::getModel('eav/entity_attribute_set');
}
if(!is_numeric($model->getAttributeSetId())){
    $new = true;
}
$model->setEntityTypeId($entityTypeId);

$model->setAttributeSetName('Cans');
$model->validate();
$model->save();

if($new){
    $model->initFromSkeleton($cloneSetId)->save();
}


$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

if(!$installer->getAttributeId('catalog_product', 'canister_condition')) {
    $installer->addAttribute('catalog_product', 'canister_condition', array(
        'attribute_set' => 'Cans',
        'label' => 'Состояние канистры',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Новые',
                1 => 'Б/у',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'canister_condition');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Cans');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'product_nomination_cans')) {
    $installer->addAttribute('catalog_product', 'product_nomination_cans', array(
        'attribute_set' => 'Cans',
        'label' => 'Наименование товара канистры',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Канистры пластиковые',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'product_nomination_cans');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Cans');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'canister_stackability')) {
    $installer->addAttribute('catalog_product', 'canister_stackability', array(
        'attribute_set' => 'Cans',
        'label' => 'Штабелируемость канистр',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Штабелируемые',
                1 => 'Не штабелируемые',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'canister_stackability');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Cans');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'canister_capacity')) {
    $installer->addAttribute('catalog_product', 'canister_capacity', array(
        'attribute_set' => 'Cans',
        'label' => 'Обьем канистры',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => '0,25',
                1 => '0,5',
                2 => '1,0',
                3 => '1,6',
                4 => '2,0',
                5 => '3,0',
                6 => '4,0',
                7 => '4,2',
                8 => '4,5',
                9 => '5,0',
                10 => '9,0',
                11 => '10,0',
                12 => '20,0',
                13 => '25,0',
                14 => '30,0',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'canister_capacity');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Cans');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'application_area_cans')) {
    $installer->addAttribute('catalog_product', 'application_area_cans', array(
        'attribute_set' => 'Cans',
        'label' => 'Область применения канистры',
        'input' => 'multiselect',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Химические продукты жидкие',
                1 => 'Нефтехимические вещества жидкие',
                2 => 'Опасные грузы жидкие',
                3 => 'Пищевые продукты жидкие',
                4 => 'Технические продукты жидкие',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'application_area_cans');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Cans');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

$installer->endSetup();
?>