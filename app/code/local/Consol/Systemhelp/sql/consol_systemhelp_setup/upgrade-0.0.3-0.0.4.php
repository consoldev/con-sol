<?php

$entityTypeId = Mage::getModel('catalog/product')->getResource()->getTypeId();

$cloneSetId = 4;

$model =Mage::getModel('eav/entity_attribute_set')
    ->getCollection()
    ->setEntityTypeFilter($entityTypeId)
    ->addFieldToFilter('attribute_set_name', 'Drums')
    ->getFirstItem();
if(!is_object($model)){
    $model = Mage::getModel('eav/entity_attribute_set');
}
if(!is_numeric($model->getAttributeSetId())){
    $new = true;
}
$model->setEntityTypeId($entityTypeId);

$model->setAttributeSetName('Drums');
$model->validate();
$model->save();

if($new){
    $model->initFromSkeleton($cloneSetId)->save();
}


$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

if(!$installer->getAttributeId('catalog_product', 'drums_condition')) {
    $installer->addAttribute('catalog_product', 'drums_condition', array(
        'attribute_set' => 'Drums',
        'label' => 'Состояние барабанов',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Новые',
                1 => 'Б/у',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'drums_condition');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Drums');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'product_nomination_drums')) {
    $installer->addAttribute('catalog_product', 'product_nomination_drums', array(
        'attribute_set' => 'Drums',
        'label' => 'Наименование товара барабаны',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Барабаны пластиковые',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'product_nomination_drums');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Drums');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'drums_type')) {
    $installer->addAttribute('catalog_product', 'drums_type', array(
        'attribute_set' => 'Drums',
        'label' => 'Вид барабанов',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Барабаны с закрытым верхом',
                1 => 'Барабаны с открывающимся верхом',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'drums_type');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Drums');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'drums_capacity')) {
    $installer->addAttribute('catalog_product', 'drums_capacity', array(
        'attribute_set' => 'Drums',
        'label' => 'Обьем барабанов',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => '50',
                1 => '80',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'drums_capacity');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Drums');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'application_area_drums')) {
    $installer->addAttribute('catalog_product', 'application_area_drums', array(
        'attribute_set' => 'Drums',
        'label' => 'Область применения канистры',
        'input' => 'multiselect',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Химические продукты жидкие',
                1 => 'Химические продукты пастообразные',
                2 => 'Химические продукты сыпучие',
                3 => 'Пищевые продукты жидкие',
                4 => 'Пищевые продукты пастообразные',
                5 => 'Пищевые продукты сыпучие',
                6 => 'Технические продукты жидкие',
                7 => 'Технические продукты пастообразные',
                8 => 'Технические продукты сыпучие',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'application_area_drums');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Drums');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};
$installer->endSetup();
?>