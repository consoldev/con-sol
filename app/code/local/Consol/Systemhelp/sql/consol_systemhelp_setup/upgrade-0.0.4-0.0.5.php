<?php

$entityTypeId = Mage::getModel('catalog/product')->getResource()->getTypeId();

$cloneSetId = 4;

$model =Mage::getModel('eav/entity_attribute_set')
    ->getCollection()
    ->setEntityTypeFilter($entityTypeId)
    ->addFieldToFilter('attribute_set_name', 'Flasks')
    ->getFirstItem();
if(!is_object($model)){
    $model = Mage::getModel('eav/entity_attribute_set');
}
if(!is_numeric($model->getAttributeSetId())){
    $new = true;
}
$model->setEntityTypeId($entityTypeId);

$model->setAttributeSetName('Flasks');
$model->validate();
$model->save();

if($new){
    $model->initFromSkeleton($cloneSetId)->save();
}


$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

if(!$installer->getAttributeId('catalog_product', 'flask_condition')) {
    $installer->addAttribute('catalog_product', 'flask_condition', array(
        'attribute_set' => 'Flasks',
        'label' => 'Состояние фляги',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Новые',
                1 => 'Б/у',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'flask_condition');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Flasks');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'product_nomination_flask')) {
    $installer->addAttribute('catalog_product', 'product_nomination_flask', array(
        'attribute_set' => 'Flasks',
        'label' => 'Наименование товара канистры',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Фляги пластиковые',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'product_nomination_flask');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Flasks');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'flask_neck')) {
    $installer->addAttribute('catalog_product', 'flask_neck', array(
        'attribute_set' => 'Flasks',
        'label' => 'Горловина',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Узкая',
                1 => 'Средняя',
                2 => 'Широкая'
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'flask_neck');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Flasks');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'flask_capacity')) {
    $installer->addAttribute('catalog_product', 'flask_capacity', array(
        'attribute_set' => 'Flasks',
        'label' => 'Обьем фляги',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => '10',
                1 => '12',
                2 => '15',
                3 => '20',
                4 => '30',
                5 => '40',
                6 => '50',
                7 => '60',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'flask_capacity');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Flasks');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'application_area_flask')) {
    $installer->addAttribute('catalog_product', 'application_area_flask', array(
        'attribute_set' => 'Flasks',
        'label' => 'Область применения фляги',
        'input' => 'multiselect',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Химические продукты жидкие',
                1 => 'Химические продукты пастообразные',
                2 => 'Химические продукты сыпучие',
                3 => 'Пищевые продукты жидкие',
                4 => 'Пищевые продукты пастообразные',
                5 => 'Пищевые продукты сыпучие',
                6 => 'Технические продукты жидкие',
                7 => 'Технические продукты пастообразные',
                8 => 'Технические продукты сыпучие',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'application_area_flask');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Flasks');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

$installer->endSetup();
?>