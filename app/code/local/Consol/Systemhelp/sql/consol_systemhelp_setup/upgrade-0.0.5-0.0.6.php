<?php

$entityTypeId = Mage::getModel('catalog/product')->getResource()->getTypeId();

$cloneSetId = 4;

$model =Mage::getModel('eav/entity_attribute_set')
    ->getCollection()
    ->setEntityTypeFilter($entityTypeId)
    ->addFieldToFilter('attribute_set_name', 'Eurocubes')
    ->getFirstItem();
if(!is_object($model)){
    $model = Mage::getModel('eav/entity_attribute_set');
}
if(!is_numeric($model->getAttributeSetId())){
    $new = true;
}
$model->setEntityTypeId($entityTypeId);

$model->setAttributeSetName('Eurocubes');
$model->validate();
$model->save();

if($new){
    $model->initFromSkeleton($cloneSetId)->save();
}


$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

if(!$installer->getAttributeId('catalog_product', 'eurocube_condition')) {
    $installer->addAttribute('catalog_product', 'eurocube_condition', array(
        'attribute_set' => 'Eurocubes',
        'label' => 'Состояние еврокубов',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Новые',
                1 => 'Б/у',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'eurocube_condition');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Eurocubes');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'product_nomination_eurocubes')) {
    $installer->addAttribute('catalog_product', 'product_nomination_eurocubes', array(
        'attribute_set' => 'Eurocubes',
        'label' => 'Наименование товара еврокуб',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Еврокубы',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'product_nomination_eurocubes');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Eurocubes');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'eurocube_neck')) {
    $installer->addAttribute('catalog_product', 'eurocube_neck', array(
        'attribute_set' => 'Eurocubes',
        'label' => 'Штабелируемость канистр',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Узкая',
                1 => 'Средняя',
                2 => 'Широкая',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'eurocube_neck');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Eurocubes');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'eurocube_capacity')) {
    $installer->addAttribute('catalog_product', 'eurocube_capacity', array(
        'attribute_set' => 'Eurocubes',
        'label' => 'Обьем еврокуба',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => '1000',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'eurocube_capacity');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Eurocubes');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'application_area_eurocube')) {
    $installer->addAttribute('catalog_product', 'application_area_eurocube', array(
        'attribute_set' => 'Eurocubes',
        'label' => 'Область применения еврокубов',
        'input' => 'multiselect',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Химические продукты жидкие',
                1 => 'Пищевые продукты жидкие',
                2 => 'Технические продукты жидкие',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'application_area_eurocube');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Eurocubes');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

$installer->endSetup();
?>