<?php

$entityTypeId = Mage::getModel('catalog/product')->getResource()->getTypeId();

$cloneSetId = 4;

$model =Mage::getModel('eav/entity_attribute_set')
    ->getCollection()
    ->setEntityTypeFilter($entityTypeId)
    ->addFieldToFilter('attribute_set_name', 'Tin_Packet')
    ->getFirstItem();
if(!is_object($model)){
    $model = Mage::getModel('eav/entity_attribute_set');
}
if(!is_numeric($model->getAttributeSetId())){
    $new = true;
}
$model->setEntityTypeId($entityTypeId);

$model->setAttributeSetName('Tin_Packet');
$model->validate();
$model->save();

if($new){
    $model->initFromSkeleton($cloneSetId)->save();
}


$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
if(!$installer->getAttributeId('catalog_product', 'product_nomination_tin_pack')) {
    $installer->addAttribute('catalog_product', 'product_nomination_tin_pack', array(
        'attribute_set' => 'Tin_Packet',
        'label' => 'Наименование товара упаковки',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Упаковка жестяная',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'product_nomination_tin_pack');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Tin_Packet');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'tin_pack_type')) {
    $installer->addAttribute('catalog_product', 'tin_pack_type', array(
        'attribute_set' => 'Tin_Packet',
        'label' => 'Тип упаковки жестяной',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Цилиндрическая',
                1 => 'Прямоугольная',
                2 => 'Коническая',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'tin_pack_type');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Tin_Packet');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'tin_capacity')) {
    $installer->addAttribute('catalog_product', 'tin_capacity', array(
        'attribute_set' => 'Tin_Packet',
        'label' => 'Обьем упаковки',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'tin_capacity');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Tin_Packet');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'tin_size')) {
    $installer->addAttribute('catalog_product', 'tin_size', array(
        'attribute_set' => 'Tin_Packet',
        'label' => 'Размер упаковки',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'tin_size');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Tin_Packet');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};

if(!$installer->getAttributeId('catalog_product', 'application_area_tin_pack')) {
    $installer->addAttribute('catalog_product', 'application_area_tin_pack', array(
        'attribute_set' => 'Tin_Packet',
        'label' => 'Область применения упаковки жестяной',
        'input' => 'multiselect',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note' => '',
        'option' => [
            'values' => [
                0 => 'Химические продукты жидкие',
                1 => 'Химические продукты пастообразные',
                2 => 'Химические продукты сыпучие',
                3 => 'Пищевые продукты жидкие',
                4 => 'Пищевые продукты пастообразные',
                5 => 'Пищевые продукты сыпучие',
                6 => 'Технические продукты жидкие',
                7 => 'Технические продукты пастообразные',
                8 => 'Технические продукты сыпучие',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'application_area_tin_pack');
    $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Tin_Packet');
    $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
};
$installer->endSetup();
?>