<?php
$cmsPage = Array (
    'title' => 'About us',
    'root_template' => 'one_column',
    'identifier' => 'about',
    'content' => "<p>about us</p>",
    'is_active' => 1,
    'stores' => array(0),
    'sort_order' => 0
);
$collection = Mage::getModel('cms/page')->getCollection()->addFieldToFilter('identifier', 'about');
$page = Mage::getModel('cms/page')->load($collection->getFirstItem()->getId());
if(!$page->getPageId()) {
    $page->setData($cmsPage)->save();
};

$cmsPage = Array (
    'title' => 'Payment',
    'root_template' => 'one_column',
    'identifier' => 'payment',
    'content' => "<p>Payment</p>",
    'is_active' => 1,
    'stores' => array(0),
    'sort_order' => 0
);

$collection = Mage::getModel('cms/page')->getCollection()->addFieldToFilter('identifier', 'payment');
$page = Mage::getModel('cms/page')->load($collection->getFirstItem()->getId());
if(!$page->getPageId()) {
    $page->setData($cmsPage)->save();
};


$cmsPage = Array (
    'title' => 'Logistics',
    'root_template' => 'one_column',
    'identifier' => 'logistics',
    'content' => "<p>Logistics</p>",
    'is_active' => 1,
    'stores' => array(0),
    'sort_order' => 0
);

$collection = Mage::getModel('cms/page')->getCollection()->addFieldToFilter('identifier', 'logistics');
$page = Mage::getModel('cms/page')->load($collection->getFirstItem()->getId());
if(!$page->getPageId()) {
    $page->setData($cmsPage)->save();
};


$cmsPage = Array (
    'title' => 'Partners',
    'root_template' => 'one_column',
    'identifier' => 'partners',
    'content' => "<p>Partners</p>",
    'is_active' => 1,
    'stores' => array(0),
    'sort_order' => 0
);

$collection = Mage::getModel('cms/page')->getCollection()->addFieldToFilter('identifier', 'partners');
$page = Mage::getModel('cms/page')->load($collection->getFirstItem()->getId());
if(!$page->getPageId()) {
    $page->setData($cmsPage)->save();
};


$cmsPage = Array (
    'title' => 'Cooperation',
    'root_template' => 'one_column',
    'identifier' => 'cooperation',
    'content' => "<p>Cooperation</p>",
    'is_active' => 1,
    'stores' => array(0),
    'sort_order' => 0
);

$collection = Mage::getModel('cms/page')->getCollection()->addFieldToFilter('identifier', 'cooperation');
$page = Mage::getModel('cms/page')->load($collection->getFirstItem()->getId());
if(!$page->getPageId()) {
    $page->setData($cmsPage)->save();
};


$cmsPage = Array (
    'title' => 'Contact',
    'root_template' => 'one_column',
    'identifier' => 'contact',
    'content' => "<p>Contact</p>",
    'is_active' => 1,
    'stores' => array(0),
    'sort_order' => 0

);
$collection = Mage::getModel('cms/page')->getCollection()->addFieldToFilter('identifier', 'contact');
$page = Mage::getModel('cms/page')->load($collection->getFirstItem()->getId());
if(!$page->getPageId()) {
    $page->setData($cmsPage)->save();
};



$cmsPage = Array (
    'title' => 'Carriers',
    'root_template' => 'one_column',
    'identifier' => 'carriers',
    'content' => "<p>Contact</p>",
    'is_active' => 1,
    'stores' => array(0),
    'sort_order' => 0

);
$collection = Mage::getModel('cms/page')->getCollection()->addFieldToFilter('identifier', 'carriers');
$page = Mage::getModel('cms/page')->load($collection->getFirstItem()->getId());
if(!$page->getPageId()) {
    $page->setData($cmsPage)->save();
};



$cmsPage = Array (
    'title' => 'Guarantees',
    'root_template' => 'one_column',
    'identifier' => 'guarantees',
    'content' => "<p>Contact</p>",
    'is_active' => 1,
    'stores' => array(0),
    'sort_order' => 0

);
$collection = Mage::getModel('cms/page')->getCollection()->addFieldToFilter('identifier', 'guarantees');
$page = Mage::getModel('cms/page')->load($collection->getFirstItem()->getId());
if(!$page->getPageId()) {
    $page->setData($cmsPage)->save();
};



$cmsPage = Array (
    'title' => 'Purchase returns',
    'root_template' => 'one_column',
    'identifier' => 'purchase_returns',
    'content' => "<p>Contact</p>",
    'is_active' => 1,
    'stores' => array(0),
    'sort_order' => 0

);
$collection = Mage::getModel('cms/page')->getCollection()->addFieldToFilter('identifier', 'purchase_returns');
$page = Mage::getModel('cms/page')->load($collection->getFirstItem()->getId());
if(!$page->getPageId()) {
    $page->setData($cmsPage)->save();
};



$cmsPage = Array (
    'title' => 'Personal cabinet',
    'root_template' => 'one_column',
    'identifier' => 'lichnyy_kabinet',
    'content' => "<p>Contact</p>",
    'is_active' => 1,
    'stores' => array(0),
    'sort_order' => 0

);
$collection = Mage::getModel('cms/page')->getCollection()->addFieldToFilter('identifier', 'lichnyy_kabinet');
$page = Mage::getModel('cms/page')->load($collection->getFirstItem()->getId());
if(!$page->getPageId()) {
    $page->setData($cmsPage)->save();
};



$cmsPage = Array (
    'title' => 'Vacancies',
    'root_template' => 'one_column',
    'identifier' => 'vacancies',
    'content' => "<p>Contact</p>",
    'is_active' => 1,
    'stores' => array(0),
    'sort_order' => 0

);
$collection = Mage::getModel('cms/page')->getCollection()->addFieldToFilter('identifier', 'vacancies');
$page = Mage::getModel('cms/page')->load($collection->getFirstItem()->getId());
if(!$page->getPageId()) {
    $page->setData($cmsPage)->save();
};


?>