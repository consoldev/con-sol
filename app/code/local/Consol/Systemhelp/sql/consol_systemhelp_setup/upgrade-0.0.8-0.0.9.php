<?php

$content = '<div class="footer-col">
                <div class="footer-col-content footer-col-left">
                    <ul>
                        <li class="footer_fax"></li>
                        <li class="footer_email"></li>
                        <li class="footer_skype"></li>
                        <li class="footer_address"><span>02081, Украина, г. Киев, ул. Здолбуновская, 7а</span></li>
                    </ul>
                </div>
            </div>
            <div class="footer-col">
                <div class="footer-col-content footer-col-center">
                    <ul>
                        <li><a href="{{store url=\'about\'}}">О Компании</a></li>
                        <li><a href="{{store url=\'payment\'}}">Оплата</a></li>
                        <li><a href="{{store url=\'logistics\'}}">Доставка</a></li>
                        <li><a href="{{store url=\'partners\'}}">Нам доверяют</a></li>
                        <li><a href="{{store url=\'cooperation\'}}">Поставщикам</a></li>
                        <li><a href="{{store url=\'contact\'}}">Контакты</a></li>
                    </ul>
                </div>
            </div>
            <div class="footer-col">
                <div class="footer-col-content footer-col-right">
                    <ul>
                        <li><a href="{{store url=\'carriers\'}}">Перевозчкам</a></li>
                        <li><a href="{{store url=\'guarantees\'}}">Гарантия от Компании</a></li>
                        <li><a href="{{store url=\'purchase_returns\'}}">Возврат Товара</a></li>
                        <li><a href="{{store url=\'lichnyy_kabinet\'}}">Личный кабинет</a></li>
                        <li><a href="{{store url=\'vacancies\'}}">Вакансии</a></li>
                    </ul>
                </div>
            </div>
           
';
//if you want one block for each store view, get the store collection
$stores = Mage::getModel('core/store')->getCollection()->addFieldToFilter('store_id', array('gt'=>0))->getAllIds();
//if you want one general block for all the store viwes, uncomment the line below
//$stores = array(0);
$loader = Mage::getModel('cms/block')->load('footer_contacts_info');
//if(!$loader) {
    foreach ($stores as $store) {
        $block = Mage::getModel('cms/block');
        $block->setTitle('Footer contacts info block');
        $block->setIdentifier('footer_contacts_info');
        $block->setStores(array($store));
        $block->setIsActive(1);
        $block->setContent($content);
        $block->save();
 //   }
}
?>