<?php

$mageFilename = 'app/Mage.php';
require_once $mageFilename;
Mage::setIsDeveloperMode(true);
ini_set('display_errors', 1);
umask(0);
Mage::app('admin');
Mage::register('isSecureArea', 1);
$parentId = '2';

try{
    $category = Mage::getModel('catalog/category');
    $category->setName('Бочки');
    $category->setUrlKey('bochki');
    $category->setIsActive(1);
    $category->setDisplayMode('PRODUCTS');
    $category->setIsAnchor(1);
    $category->setStoreId(Mage::app()->getStore()->getId());
    $parentCategory = Mage::getModel('catalog/category')->load($parentId);
    $category->setPath($parentCategory->getPath());

    if ($isChildren = $parentCategory->getChildrenCategories()){
        foreach ($isChildren as $cat){
            if ($cat->getUrlKey() != $category->getUrlKey()){
                $category->save();
            }
        }
    }
} catch(Exception $e) {
    print_r($e);
}
try{
     $category = Mage::getModel('catalog/category');
     $category->setName('Еврокубы');
     $category->setUrlKey('eurocub');
     $category->setIsActive(1);
     $category->setDisplayMode('PRODUCTS');
     $category->setIsAnchor(1);
     $category->setStoreId(Mage::app()->getStore()->getId());
     $parentCategory = Mage::getModel('catalog/category')->load($parentId);
     $category->setPath($parentCategory->getPath());

     if ($isChildren = $parentCategory->getChildrenCategories()){
         foreach ($isChildren as $cat){
             if ($cat->getUrlKey() != $category->getUrlKey()){
                $category->save();
             }
         }
     }
} catch(Exception $e) {
    print_r($e);
}
try{
    $category = Mage::getModel('catalog/category');
    $category->setName('Упаковка жестяная');
    $category->setUrlKey('tin_packing');
    $category->setIsActive(1);
    $category->setDisplayMode('PRODUCTS');
    $category->setIsAnchor(1);
    $category->setStoreId(Mage::app()->getStore()->getId());
    $parentCategory = Mage::getModel('catalog/category')->load($parentId);
    $category->setPath($parentCategory->getPath());

    if ($isChildren = $parentCategory->getChildrenCategories()){
        foreach ($isChildren as $cat){
            if ($cat->getUrlKey() != $category->getUrlKey()){
                $category->save();
            }
        }
    }
} catch(Exception $e) {
    print_r($e);
}
try{
    $category = Mage::getModel('catalog/category');
    $category->setName('Канистры');
    $category->setUrlKey('kanistry');
    $category->setIsActive(1);
    $category->setDisplayMode('PRODUCTS');
    $category->setIsAnchor(1);
    $category->setStoreId(Mage::app()->getStore()->getId());
    $parentCategory = Mage::getModel('catalog/category')->load($parentId);
    $category->setPath($parentCategory->getPath());

    if ($isChildren = $parentCategory->getChildrenCategories()){
        foreach ($isChildren as $cat){
            if ($cat->getUrlKey() != $category->getUrlKey()){
                $category->save();
            }
        }
    }
} catch(Exception $e) {
    print_r($e);
}
try{
    $category = Mage::getModel('catalog/category');
    $category->setName('Фляги');
    $category->setUrlKey('water_bottle');
    $category->setIsActive(1);
    $category->setDisplayMode('PRODUCTS');
    $category->setIsAnchor(1);
    $category->setStoreId(Mage::app()->getStore()->getId());
    $parentCategory = Mage::getModel('catalog/category')->load($parentId);
    $category->setPath($parentCategory->getPath());

    if ($isChildren = $parentCategory->getChildrenCategories()){
        foreach ($isChildren as $cat){
            if ($cat->getUrlKey() != $category->getUrlKey()){
                $category->save();
            }
        }
    }
} catch(Exception $e) {
    print_r($e);
}
try{
    $category = Mage::getModel('catalog/category');
    $category->setName('Барабаны');
    $category->setUrlKey('barrel');
    $category->setIsActive(1);
    $category->setDisplayMode('PRODUCTS');
    $category->setIsAnchor(1);
    $category->setStoreId(Mage::app()->getStore()->getId());
    $parentCategory = Mage::getModel('catalog/category')->load($parentId);
    $category->setPath($parentCategory->getPath());

    if ($isChildren = $parentCategory->getChildrenCategories()){
        foreach ($isChildren as $cat){
            if ($cat->getUrlKey() != $category->getUrlKey()){
                $category->save();
            }
        }
    }
} catch(Exception $e) {
    print_r($e);
}
try{
    $category = Mage::getModel('catalog/category');
    $category->setName('Ёмкости');
    $category->setUrlKey('capacities');
    $category->setIsActive(1);
    $category->setDisplayMode('PRODUCTS');
    $category->setIsAnchor(1);
    $category->setStoreId(Mage::app()->getStore()->getId());
    $parentCategory = Mage::getModel('catalog/category')->load($parentId);
    $category->setPath($parentCategory->getPath());

    if ($isChildren = $parentCategory->getChildrenCategories()){
        foreach ($isChildren as $cat){
            if ($cat->getUrlKey() != $category->getUrlKey()){
                $category->save();
            }
        }
    }
} catch(Exception $e) {
    print_r($e);
}
try{
    $category = Mage::getModel('catalog/category');
    $category->setName('Упаковка мягкая');
    $category->setUrlKey('packing_soft');
    $category->setIsActive(1);
    $category->setDisplayMode('PRODUCTS');
    $category->setIsAnchor(1);
    $category->setStoreId(Mage::app()->getStore()->getId());
    $parentCategory = Mage::getModel('catalog/category')->load($parentId);
    $category->setPath($parentCategory->getPath());

    if ($isChildren = $parentCategory->getChildrenCategories()){
        foreach ($isChildren as $cat){
            if ($cat->getUrlKey() != $category->getUrlKey()){
                $category->save();
            }
        }
    }
} catch(Exception $e) {
    print_r($e);
}

?>