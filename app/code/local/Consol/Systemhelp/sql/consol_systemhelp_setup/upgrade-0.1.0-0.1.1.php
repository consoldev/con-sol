<?php

$content = '<div class="footer_bot_col">
                <div class="footer-bot-col-content bot-col-content-left">
                    <ul>
                        <li class="footer_visits"></li>
                        <li class="footer_sitemap"><a href="catalog/seo_sitemap/category/"><span class="foot-icon icon-site-map"></span><span class="icon-site-map-right">Sitemap</span></a></li>

                    </ul>
                </div>
            </div>
            <div class="footer_bot_col">
                <div class="footer-bot-col-content">
                    <ul>
                        <li class="privat-icon"><a href="#"><img src="{{config path="web/secure/base_url"}}/skin/frontend/default/theme692/images/consol/footer-icon-pb2.png" title="Приват24" class="payment-methods-icon-privat"></a></li>
                        <li class="visa-icon"><a href="#"><img src="{{config path="web/secure/base_url"}}/skin/frontend/default/theme692/images/consol/footer-icon-visa1.png" title="Виза" class="payment-methods-icon-visa"></a></li>
                        <li class="mastercard-icon"><a href="#"><img src="{{config path="web/secure/base_url"}}/skin/frontend/default/theme692/images/consol/footer-icon-mc1.png" title="Мастер Кард" class="payment-methods-icon-mc"></a></li>
                        <li class="maestro-icon"><a href="#"><img src="{{config path="web/secure/base_url"}}/skin/frontend/default/theme692/images/consol/footer-icon-me1.png" title="Маэстро" class="payment-methods-icon-me"></a></li>
                    </ul>
                </div>
            </div>
            <div class="footer_bot_col">
                <div class="footer-bot-col-content">
                    <ul>
                        <li class="print-icon"><a href="#"><span class="foot-icon foot-icon-print"></span></a></li>
                        <li class="twitter-icon"><a href="#"><span class="foot-icon foot-icon-twitter"></span></a></li>
                        <li class="fb-icon"><a href="#"><span class="foot-icon foot-icon-fb"></span></a></li>
                        <li class="googleplus-icon"><a href="#"><span class="foot-icon foot-icon-googleplus"></span></a></li>
                        <li class="yourube-icon"><a href="#"><span class="foot-icon foot-icon-yourube"></span></a></li>
                        <li class="instagram-icon"><a href="#"><span class="foot-icon foot-icon-instagram"></span></a></li>
                    </ul>
                </div>
            </div>   
';
//if you want one block for each store view, get the store collection
$stores = Mage::getModel('core/store')->getCollection()->addFieldToFilter('store_id', array('gt'=>0))->getAllIds();
//if you want one general block for all the store viwes, uncomment the line below
//$stores = array(0);
$loader = Mage::getModel('cms/block')->load('bot_footer_icons');
if(!$loader) {
foreach ($stores as $store) {
    $block = Mage::getModel('cms/block');
    $block->setTitle('Footer Icons');
    $block->setIdentifier('bot_footer_icons');
    $block->setStores(array($store));
    $block->setIsActive(1);
    $block->setContent($content);
    $block->save();
}
}
?>