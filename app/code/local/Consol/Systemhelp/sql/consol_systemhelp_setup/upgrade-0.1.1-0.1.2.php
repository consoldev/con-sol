<?php

$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

if(!$installer->getAttributeId('catalog_product', 'show_on_home_page')) {
    $installer->addAttribute('catalog_product', 'show_on_home_page', array(
        'attribute_set' => 'Flasks',
        'label' => 'Show on home page',
        'input'    => 'boolean',
        'type' => 'int',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note'     => "select yes no",
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'show_on_home_page');

    $attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_set_collection');
    $attributeSetCollection->setEntityTypeFilter('4'); // 4 is Catalog Product Entity Type ID
    foreach ($attributeSetCollection as $id=>$attributeSet) {
        $attributeGroupId = $installer->getAttributeGroupId('catalog_product',  $attributeSet->getId(), 'General');
        $installer->addAttributeToSet('catalog_product', $attributeSet->getId(), $attributeGroupId, $attributeId);
    }
};

?>