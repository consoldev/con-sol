<?php

$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

if(!$installer->getAttributeId('catalog_product', 'select_units_type')) {
    $installer->addAttribute('catalog_product', 'select_units_type', array(
        'attribute_set' => 'Default',
        'label' => 'Select units type',
        'input' => 'select',
        'type' => 'varchar',
        'required' => 0,
        'visible_on_front' => 1,
        'filterable' => 1,
        'searchable' => 1,
        'comparable' => 1,
        'user_defined' => 1,
        'is_configurable' => 0,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note'     => "select units",
        'option' => [
            'values' => [
                0 => 'Items',
                1 => 'Liters',
            ]
        ],
    ));

    $attributeId = $installer->getAttributeId('catalog_product', 'select_units_type');

    $attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_set_collection');
    $attributeSetCollection->setEntityTypeFilter('4'); // 4 is Catalog Product Entity Type ID
    foreach ($attributeSetCollection as $id=>$attributeSet) {
        $attributeGroupId = $installer->getAttributeGroupId('catalog_product',  $attributeSet->getId(), 'General');
        $installer->addAttributeToSet('catalog_product', $attributeSet->getId(), $attributeGroupId, $attributeId);
    }
};
$installer->endSetup();
?>