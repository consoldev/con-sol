<?php

$content = '<div class="seo_text_block">

</div>';
//if you want one block for each store view, get the store collection
$stores = Mage::getModel('core/store')->getCollection()->addFieldToFilter('store_id', array('gt'=>0))->getAllIds();
//if you want one general block for all the store viwes, uncomment the line below
//$stores = array(0);
//$loader = Mage::getModel('cms/block')->load('seo_text_block');
//if(!$loader) {
foreach ($stores as $store) {
    $block = Mage::getModel('cms/block');
    $block->setTitle('Seo bottom block');
    $block->setIdentifier('seo_text_block');
    $block->setStores(array($store));
    $block->setIsActive(1);
    $block->setContent($content);
    $block->save();
}
//}
?>