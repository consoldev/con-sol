jQuery(document).on('click', '#zero-department', function(e) {
    jQuery('#depart_select_dropdown').slideToggle(500);
});

jQuery(document).on('click', '#bot-zero-department', function(e) {
    jQuery('#bot_depart_select_dropdown').slideToggle(500);
    if(jQuery('.padded i').css('display') === 'none'){
        jQuery('.padded i').show();
    }else{
        jQuery('.padded i').fadeOut(600);
    }
});
function ajaxCompareFadeOut() {
    jQuery('#ajax-compare-popup-shadower').fadeOut();
}
function ajaxCompare(url,id, el){
    url = url.replace("catalog/product_compare/add","contact/index/compare");
    url += 'isAjax/1/';
    jQuery('#ajax_loading'+id).show();
    jQuery.ajax( {
        url : url,
        dataType : 'json',
        success : function(data) {
            if(data.status == 'ERROR'){
                alert(data.message);
            }else{
                jQuery('#ajax-compare-popup').html(data.message);
                jQuery('#ajax-compare-popup-shadower').fadeIn();
                jQuery(el).html();
                jQuery(el).closest('li').append('<div class="added-to-compare"></div>');
                jQuery('#ajax_loading'+id).hide();
                if(jQuery('.block-compare').length){
                    jQuery('.block-compare').replaceWith(data.sidebar);
                }else{
                    if(jQuery('.col-right').length){
                        jQuery('.col-right').prepend(data.sidebar);
                    }
                }
                jQuery('#catalog-compare-container').html(data.header_compare);
                setTimeout(ajaxCompareFadeOut, 5000);
            }
        }
    });
}

function openNewWindow(link) {
    window.open(link,'pmw','scrollbars=1,top=0,left=0,resizable=1,width=880,height=550')
}

jQuery(function () {
        jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() > 100 && jQuery(window).width() > 767 ) {
                jQuery('#sticky_bot_menu').fadeIn();
            } else {
                jQuery('#sticky_bot_menu').fadeOut();
            }
        });
    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > 100) {
            jQuery('.top-cart-container').addClass('fix-top-position');
        } else {
            jQuery('.top-cart-container').removeClass('fix-top-position');
        }
    });

});

jQuery( document ).ready(function() {



    var bottomMenuHeight = jQuery('#sticky_bot_menu').height();


    jQuery(document).on('click', '#collapse_bot_menu', function(e) {



        jQuery('#bot_departments_scroll').slideToggle(500);
        jQuery('#bot_departments_scroll').css('display', 'flex');
           if(jQuery('span.bot_menu_show').css('display') === 'inline-block'){
            jQuery('span.bot_menu_show').css('display', 'none');
            jQuery('span.bot_menu_hide').css('display', 'inline-block');
            jQuery('.footer-shadow').height(jQuery('.footer-shadow').height() - bottomMenuHeight);

        }else{
            jQuery('span.bot_menu_hide').css('display', 'none');
            jQuery('span.bot_menu_show').css('display', 'inline-block');
            jQuery('.footer-shadow').height(jQuery('.footer-shadow').height() + bottomMenuHeight);
               if(jQuery(window).scrollTop() + jQuery(window).height() == jQuery(document).height()) {
                   jQuery("html, body").animate({ scrollTop: jQuery(document).height()}, 1000);
               }
           }
    });
});
jQuery(document).on('click', '#header_main_menu_more', function(e) {
        jQuery('.show-menu').hide();
        jQuery('.hide-menu').show();
        if(jQuery('#top-info-menu').css('display') === 'inline-block'){
                jQuery('#top-info-menu').css('display', 'none');
                jQuery('.hide-menu').css('display', 'none');
                jQuery('.quick-access').css('display', 'inline-block');
                jQuery('.show-menu').css('display', 'inline-block');
            }else{
                jQuery('#top-info-menu').css('display', 'inline-block');
                jQuery('.hide-menu').css('display', 'inline-block');
                jQuery('.show-menu').css('display', 'none');
                jQuery('.quick-access').css('display', 'none');
            }
});

function randomNumber(m,n){
    m = parseInt(m);
    n = parseInt(n);
    return Math.floor( Math.random() * (n - m + 1) ) + m;
}



function checkCapcha() {
    if(jQuery('#capcha_result').val() === jQuery('#confirm_capcha').val()){
        return true;
    }else{
        var aspmA = randomNumber(1,9); // генерируем число
        var aspmB = randomNumber(1,9); // генерируем число
        var sumAB = aspmA + aspmB;  // вычисляем сумму

        jQuery('#capcha_expresion span').html(aspmA + ' + ' + aspmB + ' = ');  // показываем пользователю выражение
        jQuery('#confirm_capcha').val(sumAB);  // присваиваем скрытому полю name="md5" контрольную сумму
        jQuery('#capcha_result').val('');
        jQuery('#capcha_message span').addClass('wrong_capcha');
        jQuery('#capcha_message').show();
        return false;
    }
}

jQuery( document ).ready(function() {
    jQuery('#collapse_bot_menu').click();
    var aspmA = randomNumber(1,9); // генерируем число
    var aspmB = randomNumber(1,9); // генерируем число
    var sumAB = aspmA + aspmB;  // вычисляем сумму

    jQuery('#capcha_expresion span').html(aspmA + ' + ' + aspmB + ' = ');  // показываем пользователю выражение
    jQuery('#confirm_capcha').val(sumAB);  // присваиваем скрытому полю name="md5" контрольную сумму

    var speed = 500;

    var showChar = 175;
    var ellipsestext = "...";

    jQuery('ul#nav li.level').each(function () {
        if(jQuery(this).find('span.spanchildren')){
            jQuery(this).find('span.spanchildren').parents('li.level').addClass('haschildren');
        }
    });

    jQuery('.more').each(function(e) {
        var content = jQuery(this).html();

        if(content.length > showChar) {

            var c = content.substr(0, showChar).replace( /<.*?>/g, '' );
            var h = content.substr(showChar, content.length - showChar);
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="#" class="morelink">' + moretext + '</a></span>';

            jQuery(this).html(html);
        }

    });

    jQuery(".morelink").click(function(){
        if(jQuery(this).hasClass("less")) {
            jQuery(this).removeClass("less");
            jQuery(this).html(moretext);
        } else {
            jQuery(this).addClass("less");
            jQuery(this).html(lesstext);
        }
        jQuery(this).parent().prev().toggle();
        jQuery(this).prev().toggle();
        return false;
    });

    jQuery('.slider-wrapper').each(function () {
        if(jQuery(this).find('li').length == 0){
            jQuery(this).remove();
        }
        jQuery(this).find('.count-product-slider').each(function () {
            if(jQuery(this).find('li').length== 0){
                jQuery(this).remove();
            }
        });
    });

    jQuery('#header-catalog-menu').on('mouseleave', function () {
        jQuery('#top-nav-container').slideUp(200);
    });
    /*jQuery(document).click(function (event) {
        if (jQuery(event.target).closest("#header-catalog-menu").length == '0') {*/



            if (jQuery('#top-nav-container').css('display') != 'none') {

                jQuery('#top-nav-container').hide();
            }
            if(jQuery('.sf-menu-phone').css('display', 'none')){
                jQuery('.sf-menu-phone').hide();
            }

     /*   }
    });*/

    if (jQuery(window).width() > 767 ) {
        jQuery("#header-catalog-menu").on('click', function () {

            if(jQuery('#top-nav-container').css('display') != 'block') {
                jQuery('#top-nav-container').slideDown(500);
            }else{
                jQuery('#top-nav-container').slideUp(200);
            }
        });
    }else{
        jQuery("#header-catalog-menu > span.left-catalog-title").on('click', function () {
            jQuery('.sf-menu-phone').toggle();
        });
    };

        jQuery("#catalog-wishlist-container").hover(function () {
            jQuery('#catalog_wishlist_id').addClass('catalog-desire-v2');
            jQuery('#catalog-wishlist-block-hidden').stop().slideDown(500);
        }, function () {
            jQuery('#catalog_wishlist_id').removeClass('catalog-desire-v2');
            jQuery('#catalog-wishlist-block-hidden').stop().slideUp(500);
        });
        jQuery("#catalog-compare-container").hover(function () {
            jQuery('#catalog_compare_id').addClass('catalog-compare-v2');
            jQuery('#compare-block-hidden').stop().slideDown(500);
        }, function () {
            jQuery('#catalog_compare_id').removeClass('catalog-compare-v2');
            jQuery('#compare-block-hidden').stop().slideUp(500);
        });
        jQuery("#catalog-cart-container").hover(function () {
            jQuery('#catalog-cart-container').addClass('catalog-cart-v2');
            jQuery('#catalog-cart-block-hidden').stop().slideDown(500);
        }, function () {
            jQuery('#catalog-cart-container').removeClass('catalog-cart-v2');
            jQuery('#catalog-cart-block-hidden').stop().slideUp(500);
        });



       // if (jQuery(window).width() > 991 ) {
            jQuery("#catalog-wishlist-container").hover(function () {
                jQuery('#catalog-wishlist-block-hidden').stop().slideDown(speed);
            }, function () {
                jQuery('#catalog-wishlist-block-hidden').stop().slideUp(speed);
            });
            jQuery("#catalog-compare-container").hover(function () {
                jQuery('#compare-block-hidden').stop().slideDown(speed);
            }, function () {
                jQuery('#compare-block-hidden').stop().slideUp(speed);
            });
            jQuery("#catalog-cart-container").hover(function () {
                jQuery('#catalog-cart-block-hidden').stop().slideDown(speed);
            }, function () {
                jQuery('#catalog-cart-block-hidden').stop().slideUp(speed);
            });

        if (jQuery(window).width() < 767 ) {

            /*jQuery("#header-catalog-menu").on('click', function () {
                jQuery(this).toggleClass('header-catalog-menu-active-mobile');
            });*/

            jQuery(".left-catalog-title").on('click', function () {
                jQuery("#header-catalog-menu").toggleClass('header-mobile-position');
            });
            jQuery('.header-center').find(".department-selector").on('click', function () {
                jQuery('#zero-department').toggleClass('zero-department-mobile');
                jQuery('#depart_select_dropdown').toggleClass('depart_select_dropdown-mobile');

            });
           /* jQuery("#bot_departments_scroll").find('.department-selector').on('click', function () {
                jQuery('#bot-zero-department').toggleClass('bot-zero-department-mobile');
                jQuery('#bot_depart_select_dropdown').toggleClass('depart_select_dropdown-mobile');
            });*/
        }


    jQuery(".carousel-prev-beginning").on('click', function(){
        jQuery('.caroufredsel-pagination a').first().trigger('click');
        return false;
    });
    jQuery(".carousel-next-end").on('click', function(){
        jQuery('.caroufredsel-pagination a').last().trigger('click');
        return false;
    });


    jQuery('li.level1.first').hover(function() {
        jQuery(this).parentsUntil('li.haschildren > a').find('span.spanchildren').addClass('spanchildren-two');
    }, function () {
        jQuery(this).parentsUntil('li.haschildren > a').find('span.spanchildren').removeClass('spanchildren-two');
    });

    jQuery('.first-menu-item-hover').hover(function() {
        if(!(jQuery(this).hasClass('left-menu-two'))) {
            jQuery(this).parentsUntil('li.haschildren > a').find('span.spanchildren').addClass('spanchildren-two');
       }
    }, function () {
        jQuery(this).parentsUntil('li.haschildren > a').find('span.spanchildren').removeClass('spanchildren-two');
    });

});
